<?php

namespace Drupal\helper;

/**
 * Provides helpers for working with HTML.
 */
class Html {

  /**
   * Remove HTML comments from a string.
   *
   * @param mixed $markup
   *   The markup.
   *
   * @return string|null
   *   The markup without HTML comments as a string.
   */
  public static function removeComments(mixed $markup): ?string {
    return preg_replace('/<!--(.|\s)*?-->\s*/s', '', (string) $markup);
  }

}
