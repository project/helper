<?php

namespace Drupal\helper;

use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

class EntityType {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * EntityType constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get a list of entity options for a select field.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param array|null $properties
   *   An optional array of properties to use when loading the entities.
   *
   * @return array|\Drupal\Core\StringTranslation\TranslatableMarkup[]|string[]
   *   The list of entities keyed by entity ID.
   */
  public function getEntityOptions(string $entity_type_id, array $properties = NULL): array {
    if (!isset($properties)) {
      $entityType = $this->entityTypeManager->getDefinition($entity_type_id);
      if ($entityType instanceof ConfigEntityTypeInterface) {
        $properties['status'] = TRUE;
      }
      elseif ($entityType->hasKey('status')) {
        $properties[$entityType->getKey('status')] = TRUE;
      }
    }
    $entities = $this->entityTypeManager->getStorage($entity_type_id)->loadByProperties($properties);
    $labels = array_map(function (EntityInterface $entity) {
      return $entity->label();
    }, $entities);
    asort($labels);
    return $labels;
  }

}
