<?php

declare(strict_types=1);

namespace Drupal\helper;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionListInterface;

/**
 * Provides helpers for working with Layout Builder.
 */
class LayoutBuilder {

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * The UUID generator.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected UuidInterface $uuid;

  /**
   * LayoutBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository
   *   The entity display repository.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID generator.
   */
  public function __construct(EntityDisplayRepositoryInterface $entityDisplayRepository, UuidInterface $uuid) {
    $this->entityDisplayRepository = $entityDisplayRepository;
    $this->uuid = $uuid;
  }

  /**
   * Checks if an entity is using layout builder or not.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   * @param bool $check_if_can_override
   *   A boolean to check if the layout can also be overridden for the entity.
   *
   * @return bool
   *   TRUE if the entity has layout builder enabled.
   */
  public function isEntityLayoutEnabled(FieldableEntityInterface $entity, bool $check_if_can_override = FALSE): bool {
    $display = $this->entityDisplayRepository->getViewDisplay($entity->getEntityTypeId(), $entity->bundle());
    return $display instanceof LayoutBuilderEntityViewDisplay
      && $display->isLayoutBuilderEnabled() &&
      (!$check_if_can_override || ($display->isOverridable() && $entity->hasField(OverridesSectionStorage::FIELD_NAME)));
  }

  /**
   * Checks if an entity is using an overridden layout builder or not.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   TRUE if the entity has an overridden layout.
   */
  public function isEntityLayoutOverridden(FieldableEntityInterface $entity): bool {
    return $this->isEntityLayoutEnabled($entity, TRUE) && !$entity->get(OverridesSectionStorage::FIELD_NAME)->isEmpty();
  }

  /**
   * Updates the overriden layout for an entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   * @param array $data
   *   The layout data.
   */
  public function setEntityLayout(FieldableEntityInterface $entity, array $data): void {
    if (!$this->isEntityLayoutEnabled($entity, TRUE)) {
      throw new \InvalidArgumentException("The entity {$entity->getEntityTypeId()} {$entity->id()} cannot override layouts.");
    }

    $sections = [];
    foreach ($data as $section) {
      $weight = 0;
      foreach ($section['components'] as &$component) {
        $component += [
          'uuid' => $this->uuid->generate(),
          'configuration' => [],
          'additional' => [],
          'weight' => $weight++,
        ];
      }

      $sections[] = Section::fromArray($section);
    }

    $entity->set(OverridesSectionStorage::FIELD_NAME, $sections);
  }

  /**
   * Checks if a layout builder section list has a plugin.
   *
   * @param \Drupal\layout_builder\SectionListInterface $sectionList
   *   The Layout builder section list.
   * @param string $plugin_id
   *   The plugin ID to find.
   *
   * @return \Drupal\layout_builder\SectionComponent[]
   *   The section components that matched.
   */
  public static function sectionListGetComponentsByPluginId(SectionListInterface $sectionList, string $plugin_id): array {
    $components = [];
    foreach ($sectionList->getSections() as $section) {
      foreach ($section->getComponents() as $component) {
        $component_plugin_id = $component->getPluginId();
        if ($component_plugin_id === $plugin_id || (str_ends_with($plugin_id, ':') && str_starts_with($component_plugin_id, $plugin_id))) {
          $components[] = $component;
        }
      }
    }
    return $components;
  }

  /**
   * Extract plugins from a section list.
   *
   * @param \Drupal\layout_builder\SectionListInterface $sectionList
   *   The Layout builder section list.
   * @param bool $plugin_instance
   *   To return the plugins as objects instead of IDs.
   *
   * @return array
   *   A nested array of layout and block plugin IDs.
   */
  public static function extractSectionListPlugins(SectionListInterface $sectionList, bool $plugin_instance = FALSE): array {
    $plugins = [
      'core.layout' => [],
      'block' => [],
    ];
    foreach ($sectionList->getSections() as $section) {
      $plugins['core.layout'][] = $plugin_instance ? $section->getLayout() : $section->getLayoutId();
      foreach ($section->getComponents() as $component) {
        $plugins['block'][] = $plugin_instance ? $component->getPlugin() : $component->getPluginId();
      }
    }
    return $plugins;
  }

}
