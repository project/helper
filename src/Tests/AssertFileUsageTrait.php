<?php

declare(strict_types=1);

use Drupal\Core\Entity\EntityInterface;
use Drupal\file\Entity\File;

/**
 * Provides assertions for file usage.
 */
trait AssertFileUsageTrait {

  /**
   * Assert file usage.
   *
   * @param int $expectedUsage
   *   The expected usage count.
   * @param int $fid
   *   The file ID to check for usage.
   * @param string $module
   *   The module to check for usage.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The attached entity to check for usage.
   */
  protected function assertFileUsage(int $expectedUsage, int $fid, string $module, EntityInterface $entity): void {
    $file = File::load($fid);
    $usage = \Drupal::service('file.usage')->listUsage($file);
    $usage = $usage[$module][$entity->getEntityTypeId()][$entity->id()] ?? 0;
    $this->assertSame($expectedUsage, (int) $usage);
  }

}
