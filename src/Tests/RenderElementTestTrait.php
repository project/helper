<?php

declare(strict_types=1);

namespace Drupal\helper\Tests;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\KernelTests\AssertContentTrait;

/**
 * Trait for helping test render elements.
 */
trait RenderElementTestTrait {

  use AssertContentTrait;

  /**
   * Renders a render array.
   *
   * @param array $elements
   *   The elements to render.
   *
   * @return string
   *   The rendered string output (typically HTML).
   */
  protected function renderElement(array &$elements): string {
    $content = (string) $this->container->get('renderer')->renderRoot($elements);
    $this->setRawContent($content);
    $this->removeWhiteSpace();
    return $this->content;
  }

  /**
   * {@inheritdoc}
   */
  protected function removeWhiteSpace(): void {
    parent::removeWhiteSpace();
    $this->content = trim(preg_replace(['/[\r\n]+/m', '/ +/', '/^ +/m', '/ +$/m'], [PHP_EOL, ' ', '', ''], $this->content));
  }

  /**
   * Remove any newlines and spaces around them to just a single space.
   */
  protected function removeContentNewlines(): void {
    // Update multiple newlines and spaces to just a single space.
    $this->content = preg_replace(['/\s+/m', '/\s+/'], [' ', ' '], $this->content);
  }

  /**
   * Rewrite any absolute and relative file URLs to their short URIs.
   */
  protected function updateFileUrlsToUri(): void {
    $wrappers = \Drupal::service('stream_wrapper_manager')->getNames(StreamWrapperInterface::VISIBLE);
    foreach (array_keys($wrappers) as $wrapper) {
      $this->content = str_replace(
        [
          \Drupal::service('file_url_generator')->generateAbsoluteString($wrapper . '://'),
          \Drupal::service('file_url_generator')->generateString($wrapper . '://'),
        ],
        $wrapper . '://',
        $this->content,
      );
    }
  }

  /**
   * Assert rendering a render array.
   *
   * @param string|\Exception $expected
   *   The expected markup HTML, or the exception to be expected. We cannot
   *   test for both expected markup and warnings since PHPUnit halts on
   *   any kind of exception, error, warning, or notice.
   * @param array $element
   *   The element's render array.
   * @param array|null $expected_cache
   *   The expected array of values in the $element['#cache'] after rendering,
   *   or an exception to be expected. It is not currently possible to test
   *   for both expected exceptions and expected cache at the same time since
   *   exceptions in PHPUnit end the test run.
   */
  public function assertRenderElement(string|\Exception $expected, array $element, ?array $expected_cache = NULL): void {
    if ($expected instanceof \Exception) {
      $this->expectExceptionObject($expected);
    }

    $this->assertSame($expected, $this->renderElement($element));

    if (isset($expected_cache)) {
      $this->assertRenderCacheMetadata($expected_cache, $element);
    }
  }

  /**
   * Test if a render element has the expected cache metadata after rendering.
   *
   * @param array|\Drupal\Core\Cache\CacheableDependencyInterface $expected_cache
   *   The expected values in the $element['#cache'] after rendering.
   * @param array $element
   *   The element's render array, once it has been rendered.
   */
  public function assertRenderCacheMetadata(array|CacheableDependencyInterface $expected_cache, array $element): void {
    $this->assertArrayHasKey('#cache', $element);
    if ($expected_cache instanceof CacheableDependencyInterface) {
      $expected_cache = [
        'contexts' => $expected_cache->getCacheContexts(),
        'tags' => $expected_cache->getCacheTags(),
      ];
    }
    if (isset($expected_cache['contexts'])) {
      $this->assertArrayHasKey('contexts', $element['#cache']);
      $this->assertEmpty(array_diff($expected_cache['contexts'], $element['#cache']['contexts']));
    }
    if (isset($expected_cache['tags'])) {
      $this->assertArrayHasKey('tags', $element['#cache']);
      $this->assertEmpty(array_diff($expected_cache['tags'], $element['#cache']['tags']));
    }
  }

}
