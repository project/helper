<?php

namespace Drupal\helper\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Select;

/**
 * Provides a element for a entity select menu.
 *
 * @FormElement("helper_entity_select")
 */
class EntitySelect extends Select {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#target_type'] = NULL;
    $info['#target_bundles'] = NULL;
    $info['#selection_handler'] = 'default';
    $info['#selection_settings'] = [];
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public static function processSelect(&$element, FormStateInterface $form_state, &$complete_form) {
    static::setEntityOptions($element);
    $element = parent::processSelect($element, $form_state, $complete_form);

    // Must convert this element['#type'] to a 'select' to prevent
    // "Illegal choice %choice in %name element" validation error.
    // @see \Drupal\Core\Form\FormValidator::performRequiredValidation
    $element['#type'] = 'select';

    // Add class.
    $element['#attributes']['class'][] = 'helper-entity-select';

    return $element;
  }

  /**
   * Set referencable entities as options for an element.
   *
   * @param array $element
   *   An element.
   * @param array $settings
   *   An array of settings used to limit and randomize options.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown when the current user doesn't have access to the specified entity.
   *
   * @see \Drupal\system\Controller\EntityAutocompleteController
   */
  public static function setEntityOptions(array &$element, array $settings = []) {
    if (!empty($element['#options'])) {
      return;
    }

    // Make sure #target_type is not empty.
    if (empty($element['#target_type'])) {
      $element['#options'] = [];
      return;
    }

    $selection_settings = $element['#selection_settings'] ?? [];
    $selection_handler_options = [
      'target_type' => $element['#target_type'],
      'target_bundles' => $element['#target_bundles'] ?? NULL,
      'handler' => $element['#selection_handler'],
    ] + $selection_settings;

    // Make sure settings has a limit.
    $settings += ['limit' => 0];

    /** @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selection_manager */
    $selection_manager = \Drupal::service('plugin.manager.entity_reference_selection');
    $handler = $selection_manager->getInstance($selection_handler_options);
    $options = $handler->getReferenceableEntities(NULL, 'CONTAINS', $settings['limit']);

    // Rebuild the array by changing the bundle key into the bundle label.
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($element['#target_type']);

    $return = [];
    foreach ($options as $bundle => $entity_ids) {
      // The label does not need sanitizing since it is used as an optgroup
      // which is only supported by select elements and auto-escaped.
      $bundle_label = (string) $bundles[$bundle]['label'];
      $return[$bundle_label] = $entity_ids;
    }

    $element['#options'] = count($return) === 1 ? reset($return) : $return;
  }

}
