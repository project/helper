<?php

namespace Drupal\helper;

use Drupal\Component\Utility\Unicode;
use DrupalEnvironment\Environment;

/**
 * Provides various utility helpers.
 */
class Utility {

  /**
   * Registers a unique function call for execution on shutdown.
   *
   * Wrapper for drupal_register_shutdown_function() that does not add the
   * function call if it already exists in the shutdown function stack.
   *
   * @param callable $callback
   *   The shutdown function to register.
   * @param mixed $args
   *   Additional arguments to pass to the shutdown function.
   *
   * @return bool
   *   TRUE if the function was added, or FALSE if it was already in the stack.
   *
   * @see drupal_register_shutdown_function()
   */
  public static function registerUniqueShutdownFunction(callable $callback, ...$args) {
    $existing_callbacks = drupal_register_shutdown_function();
    foreach ($existing_callbacks as $existing_callback) {
      if ($existing_callback['callback'] === $callback && $existing_callback['arguments'] === $args) {
        return FALSE;
      }
    }

    drupal_register_shutdown_function($callback, ...$args);
    return TRUE;
  }

  /**
   * Runs a batch even if another batch is currently running.
   *
   * This is useful for running a batch inside another batch process.
   *
   * @param array $batch
   *   A batch array that would normally get passed to batch_set().
   */
  public static function runBatch(array $batch) {
    $existing_batch = batch_get();
    $current_batch = &batch_get();
    if ($existing_batch) {
      $current_batch = NULL;
    }
    batch_set($batch);
    $current_batch['progressive'] = FALSE;
    batch_process();
    if ($existing_batch) {
      $current_batch = $existing_batch;
    }
  }

  /**
   * Runs a composer command.
   *
   * Note: USE THIS AT YOUR OWN RISK. YOU ARE RESPONSIBLE FOR ESCAPING THE
   * INPUT TO THE $command PARAMETER.
   *
   * @param string $command
   *   The composer command to run, without the "composer" part.
   * @param string $additional
   *   The additional command parameters and arguments.
   * @param mixed $result_code
   *   The result code from running the command, modified by reference.
   *
   * @throws \RuntimeException
   */
  public static function execCommand(string $command, string $additional, mixed &$result_code = NULL): void {
    if (Environment::commandExists($command)) {
      // @todo This should be using shell_exec().
      $result = passthru("$command $additional", $result_code);
      if ($result === FALSE || $result_code) {
        throw new \RuntimeException("Failed running {$command} {$additional}.");
      }
    }
    else {
      throw new \RuntimeException("The {$command} command does not exist.");
    }
  }

  /**
   * Call a plugin callback and return its result.
   *
   * @param string $plugin_type
   *   The plugin type.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $callback
   *   The method to call on the plugin class.
   * @param mixed ...$args
   *   The arguments to pass to the plugin callback.
   *
   * @return mixed
   *   The return value from the plugin method.
   *
   * @deprecated in helper:8.x-1.36 and is removed from helper:2.0.0. Use
   *   \Drupal\helper\Plugin::invoke() instead.
   *
   * @see https://www.drupal.org/project/helper/issues/3393325
   */
  public static function callPlugin(string $plugin_type, string $plugin_id, array $configuration, string $callback, ...$args) {
    return Plugin::invoke($plugin_type, $plugin_id, $configuration, $callback, ...$args);
  }

  /**
   * Convert a string to CamelCase.
   *
   * @param string $string
   *   The string.
   * @param bool $lower
   *   If the first character should be lower case (i.e. lowerCamelCase).
   *
   * @return string
   *   The string.
   */
  public static function camelCase(string $string, bool $lower = FALSE): string {
    $string = preg_replace('/[\W_]/', ' ', $string);
    $string = str_replace(' ', '', Unicode::ucwords($string));
    if ($lower) {
      $string = Unicode::lcfirst($string);
    }
    return $string;
  }

}
