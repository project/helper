<?php

declare(strict_types=1);

namespace Drupal\helper\Entity;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Utility\Error;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A utility class to make updating content entities simple.
 *
 * Use this in a post update function like so:
 * @code
 * // Ensure Taxonomy module installed before trying to update vocabularies.
 * if (\Drupal::moduleHandler()->moduleExists('taxonomy')) {
 *   // Update all the names of taxonomy terms to lower case.
 *   \Drupal::classResolver(ContentEntityUpdater::class)->update($sandbox, 'taxonomy_term', static function (TaxonomyTermInterface $term) {
 *     $name = $term->getName();
 *     $updated_name = mb_strtolower($name);
 *     if ($name !== $updated_name) {
 *       $term->setName($updated_name);
 *       // Returning TRUE will cause the entity to be saved. No need to do it here.
 *       return TRUE;
 *     }
 *   });
 * }
 * @endcode
 *
 * The number of entities processed in each batch is determined by the
 * 'entity_update_batch_size' setting.
 *
 * @see default.settings.php
 * @see \Drupal\Core\Config\Entity\ConfigEntityUpdater
 */
class ContentEntityUpdater implements ContainerInjectionInterface {

  /**
   * The key used to store information in the update sandbox.
   */
  const SANDBOX_KEY = 'content_entity_updater';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The number of entities to process in each batch.
   *
   * @var int
   */
  protected $batchSize;

  /**
   * ContentEntityUpdater constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param int $batch_size
   *   The number of entities to process in each batch.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger, $batch_size) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->batchSize = $batch_size;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('update'),
      $container->get('settings')->get('entity_update_batch_size', 50)
    );
  }

  /**
   * Updates content entities as part of a Drupal update.
   *
   * @param array $sandbox
   *   Stores information for batch updates.
   * @param string|\Drupal\Core\Entity\Query\QueryInterface $entity_type_id_or_query
   *   The content entity type ID. For example, 'node' or 'taxonomy_term'.
   *   The calling code should ensure that the entity type exists beforehand
   *   (i.e., by checking that the entity type is defined or that the module
   *   that provides it is installed).
   *   This can also be an entity query.
   *   This can also be a string in the format of 'node:bundle' to only load
   *   a specific bundle of entities. Multiple bundles can be used by using
   *   the 'node:bundle1:bundle2' string or passing an entity query object.
   * @param callable $callback
   *   A callback to determine if a content entity should be saved. The
   *   callback will be passed each entity of the provided type that exists.
   *   The callback should not save an entity itself. Return TRUE to save an
   *   entity. The callback can make changes to an entity. Note that all
   *   changes should comply with schema as an entity's data will not be
   *   validated against schema on save to avoid unexpected errors.
   * @param bool $continue_on_error
   *   Set to TRUE to continue updating if an error has occurred.
   *
   * @see hook_post_update_NAME()
   *
   * @api
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|null
   *   An error message if $continue_on_error is set to TRUE and an error has
   *   occurred.
   *
   * @throws \InvalidArgumentException
   *   Thrown when the provided entity type ID is not a content entity
   *   type.
   * @throws \RuntimeException
   *   Thrown when used twice in the same update function for different entity
   *   types. This method should only be called once per update function.
   */
  public function update(array &$sandbox, string|QueryInterface $entity_type_id_or_query, callable $callback, bool $continue_on_error = FALSE): ?TranslatableMarkup {
    $entity_type_id = $entity_type_id_or_query;
    if ($entity_type_id_or_query instanceof QueryInterface) {
      $entity_type_id = $entity_type_id_or_query->getEntityTypeId();
    }
    elseif (str_contains($entity_type_id_or_query, ':')) {
      $bundles = explode(':', $entity_type_id_or_query);
      $entity_type_id = array_shift($bundles);
    }

    $storage = $this->entityTypeManager->getStorage($entity_type_id);

    if (isset($sandbox[self::SANDBOX_KEY]) && $sandbox[self::SANDBOX_KEY]['entity_type'] !== $entity_type_id) {
      throw new \RuntimeException('Updating multiple entity types in the same update function is not supported');
    }
    if (!isset($sandbox[self::SANDBOX_KEY])) {
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      if (!($entity_type instanceof ContentEntityTypeInterface)) {
        throw new \InvalidArgumentException("The provided entity type ID '$entity_type_id' is not a content entity type");
      }
      $query = $entity_type_id_or_query;
      if (!($query instanceof QueryInterface)) {
        $query = $storage->getQuery();
        $query->accessCheck(FALSE);
        if (isset($bundles) && $bundle_key = $storage->getEntityType()->getKey('bundle')) {
          $query->condition($bundle_key, $bundles, 'IN');
        }
      }
      $sandbox[self::SANDBOX_KEY]['entity_type'] = $entity_type_id;
      $sandbox[self::SANDBOX_KEY]['entities'] = $query->execute();
      $sandbox[self::SANDBOX_KEY]['count'] = count($sandbox[self::SANDBOX_KEY]['entities']);
      $sandbox[self::SANDBOX_KEY]['failed_entity_ids'] = [];
    }

    /** @var \Drupal\Core\Entity\ContentEntityInterface[] $entities */
    $entities = $storage->loadMultiple(array_splice($sandbox[self::SANDBOX_KEY]['entities'], 0, $this->batchSize));
    foreach ($entities as $entity) {
      try {
        if ($continue_on_error) {
          // If we're continuing on error silence errors from notices that
          // missing indexes.
          // @todo consider change this to an error handler that converts such
          //   notices to exceptions in https://www.drupal.org/node/3309886
          @$this->doOne($entity, $callback);
        }
        else {
          $this->doOne($entity, $callback);
        }
      }
      catch (\Throwable $throwable) {
        if (!$continue_on_error) {
          throw $throwable;
        }
        $context['%view'] = $entity->id();
        $context['%entity_type'] = $entity_type_id;
        $context += Error::decodeException($throwable);
        $this->logger->error('Unable to update %entity_type %view due to error @message %function (line %line of %file). <pre>@backtrace_string</pre>', $context);
        $sandbox[self::SANDBOX_KEY]['failed_entity_ids'][] = $entity->id();
      }
    }

    // Clear the entity cache to prevent memory limit errors.
    $storage->resetCache();

    $sandbox['#finished'] = empty($sandbox[self::SANDBOX_KEY]['entities']) ? 1 : ($sandbox[self::SANDBOX_KEY]['count'] - count($sandbox[self::SANDBOX_KEY]['entities'])) / $sandbox[self::SANDBOX_KEY]['count'];

    if (!empty($sandbox[self::SANDBOX_KEY]['failed_entity_ids'])) {
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      return new TranslatableMarkup("Updates failed for the entity type %entity_type, for %entity_ids. Check the logs.", [
        '%entity_type' => $entity_type->getLabel(),
        '%entity_ids' => implode(', ', $sandbox[self::SANDBOX_KEY]['failed_entity_ids']),
      ]);
    }

    return NULL;
  }

  /**
   * Apply the callback an entity and save it if the callback makes changes.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to potentially update.
   * @param callable $callback
   *   The callback to apply.
   */
  protected function doOne(ContentEntityInterface $entity, callable $callback): void {
    if (call_user_func($callback, $entity)) {
      $entity->save();
    }
  }

}
