<?php

namespace Drupal\helper\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block that renders a node field.
 *
 * @Block(
 *   id = "helper_node_field",
 *   admin_label = @Translation("Node field"),
 *   category = @Translation("Content"),
 *   context_definitions = {
 *     "entity" = @ContextDefinition("entity:node", required = TRUE, label = @Translation("Entity")),
 *   },
 * )
 */
class NodeField extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityTypeBundleInfo = $container->get('entity_type.bundle.info');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'field_name' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);

    $fields = $this->entityFieldManager->getFieldStorageDefinitions('node');
    $field_options = array_map(static function (FieldStorageDefinitionInterface $field) {
      return $field->getLabel();
    }, $fields);

    $form['field_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Field'),
      '#options' => $field_options,
      '#default_value' => $this->configuration['field_name'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['field_name'] = $form_state->getValue('field_name');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    if ($field = $this->getField()) {
      if (!$field->isEmpty()) {
        $build = $field->view($this->getFieldDisplay());
      }

      // Merge in the cache metadata from the menu itself.
      $metadata = CacheableMetadata::createFromRenderArray($build);
      $metadata = $metadata->merge(CacheableMetadata::createFromObject($field->getEntity()));
      $metadata = $metadata->merge(CacheableMetadata::createFromObject($field));
      $metadata->applyTo($build);
    }

    return $build;
  }

  /**
   * Gets the field item.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|null
   *   The field item list if an entity is available or NULL otherwise.
   */
  protected function getField(): ?FieldItemListInterface {
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    if (($entity = $this->getContextValue('entity')) && $entity instanceof FieldableEntityInterface && $entity->hasField($this->configuration['field_name'])) {
      return $entity->get($this->configuration['field_name']);
    }
    return NULL;
  }

  /**
   * Get the field display array.
   *
   * @see \Drupal\Core\Entity\EntityViewBuilderInterface::viewField
   *
   * @return array
   *   The field display.
   */
  protected function getFieldDisplay(): array {
    return ['label' => 'hidden'];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    if ($this->configuration['field_name']) {
      $dependencies['config'][] = 'field.storage.node.' . $this->configuration['field_name'];
    }
    return $dependencies;
  }

}
