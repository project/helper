<?php

namespace Drupal\helper;

use Drupal\filter\FilterFormatInterface;

/**
 * Helper service for modifying text formats.
 */
class TextFormat {

  /**
   * Add a tag to the HTML restrictions for specific formats, or all.
   *
   * @param \Drupal\filter\FilterFormatInterface $format
   *   The filter format.
   * @param string $tag
   *   The tag name to add, without enclosing braces. Example: span.
   * @param array $attributes
   *   (Optional) An array of attributes to add to the tag.
   */
  public function addTag(FilterFormatInterface $format, string $tag, array $attributes = []): void {
    $restrictions = $format->getHTMLRestrictions();
    $config = $format->filters('filter_html')->getConfiguration();

    // Only modify the allowed HTML if filter_html is enabled.
    /** @var \Drupal\filter\Plugin\FilterBase $filter_html */
    $filter_html = $format->filters('filter_html');
    if ($filter_html && $filter_html->status) {
      // Check if this tag already exists.
      if (isset($restrictions['allowed'][$tag])) {
        $tag_attrs = $restrictions['allowed'][$tag];

        // If we have no new attributes then there is nothing to do here.
        if (empty($attributes)) {
          return;
        }

        // Loop through each attribute and check if it exists. If not, add it.
        foreach ($attributes as $attribute) {
          if (!isset($tag_attrs[$attribute])) {
            $tag_attrs[$attribute] = TRUE;
          }
        }
        // Update the allowed tag.
        $config['settings']['allowed_html'] = preg_replace(
          $this->buildTagRegex($tag),
          $this->buildReplacement($tag, array_keys($tag_attrs)),
          $config['settings']['allowed_html'],
          1
        );
      }
      else {
        $restriction = $this->buildReplacement($tag, $attributes);
        $config['settings']['allowed_html'] .= ' ' . $restriction;
      }

      // Update filter with new config.
      $format->setFilterConfig('filter_html', $config);
    }
  }

  /**
   * Remove a tag and all its attributes from the HTML restrictions.
   *
   * @param \Drupal\filter\FilterFormatInterface $format
   *   The filter format.
   * @param string $tag
   *   The tag name to remove, without enclosing braces. Example: span.
   */
  public function removeTag(FilterFormatInterface $format, string $tag): void {
    $restrictions = $format->getHTMLRestrictions();
    $config = $format->filters('filter_html')->getConfiguration();

    // Only modify the allowed HTML if filter_html is enabled.
    /** @var \Drupal\filter\Plugin\FilterBase $filter_html */
    $filter_html = $format->filters('filter_html');
    if ($filter_html && $filter_html->status) {
      // First check if this tag is actually allowed.
      if (isset($restrictions['allowed'][$tag])) {
        // Update the allowed tag.
        $config['settings']['allowed_html'] = preg_replace(
          $this->buildTagRegex($tag),
          '',
          $config['settings']['allowed_html'],
          1
        );

        // Update filter with new config.
        $format->setFilterConfig('filter_html', $config);
      }
    }
  }

  /**
   * Remove a tag's attributes from the HTML restrictions for formats.
   *
   * @param \Drupal\filter\FilterFormatInterface $format
   *   The filter format.
   * @param string $tag
   *   The tag name from which to remove attributes, without enclosing braces.
   * @param array $attributes
   *   An array of attributes to remove from the tag.
   */
  public function removeTagAttributes(FilterFormatInterface $format, string $tag, array $attributes): void {
    $restrictions = $format->getHTMLRestrictions();
    $config = $format->filters('filter_html')->getConfiguration();

    // Only modify the allowed HTML if filter_html is enabled.
    /** @var \Drupal\filter\Plugin\FilterBase $filter_html */
    $filter_html = $format->filters('filter_html');
    if ($filter_html && $filter_html->status) {
      // First check if this tag is actually allowed.
      if (isset($restrictions['allowed'][$tag])) {
        $tag_attrs = $restrictions['allowed'][$tag];

        // Loop through each attribute and check if it exists. If not, skip.
        foreach ($attributes as $attribute) {
          // Is this a wildcard attribute name, if so, treat it differently.
          if (strpos($attribute, '*') !== FALSE) {
            $attribute_substring = str_replace('*', '', $attribute);
            foreach ($tag_attrs as $attr_name => $status) {
              // If the wildcard exists at the start of any of the attributes,
              // then remove them.
              if (strpos($attr_name, $attribute_substring) === 0) {
                unset($tag_attrs[$attr_name]);
              }
            }
          }
          // Remove this attribute from this tag.
          if (isset($tag_attrs[$attribute])) {
            unset($tag_attrs[$attribute]);
          }
        }

        // Update the allowed tag.
        $config['settings']['allowed_html'] = preg_replace(
          $this->buildTagRegex($tag),
          $this->buildReplacement($tag, array_keys($tag_attrs)),
          $config['settings']['allowed_html'],
          1
        );

        // Update filter with new config.
        $format->setFilterConfig('filter_html', $config);
      }
    }
  }

  /**
   * Build a regex to extract a tag from the HTML restrictions string.
   *
   * @param string $tag
   *   The tag name to build the regex for, without enclosing braces.
   */
  protected function buildTagRegex(string $tag): string {
    return '/<' . preg_quote($tag, '/') . '([^>]*)>/';
  }

  /**
   * Build a replacement string for a tag and its attributes.
   *
   * @param string $tag
   *   The tag name without enclosing braces. Example: span.
   * @param array $attributes
   *   (Optional) An array of attributes to add to the tag.
   */
  protected function buildReplacement(string $tag, array $attributes = []): string {
    $replacement = '<' . $tag;
    if (!empty($attributes)) {
      $replacement .= ' ' . implode(' ', $attributes);
    }
    $replacement .= '>';
    return $replacement;
  }

  /**
   * Checks if a tag is allowed.
   *
   * @param \Drupal\filter\FilterFormatInterface $format
   *   The text format.
   * @param string $tag
   *   The HTML tag name.
   *
   * @return bool
   *   TRUE if the tag is allowed by the text format, or FALSE otherwise.
   */
  public function isTagAllowed(FilterFormatInterface $format, string $tag): bool {
    $restrictions = $format->getHtmlRestrictions();
    if ($restrictions === FALSE) {
      // Format has no restrictions.
      return TRUE;
    }

    return isset($restrictions['allowed'][$tag]);
  }

  /**
   * Checks if a tag attribute is allowed.
   *
   * @param \Drupal\filter\FilterFormatInterface $format
   *   The text format.
   * @param string $tag
   *   The HTML tag name or '*' to check this global allowed attributes.
   * @param string $attribute
   *   The optional attribute to check for.
   *
   * @return bool|null
   *   A boolean if the tag is allowed or not. A NULL result is possible when
   *   checking the global allowed attributes and there is no match found.
   */
  public function isAttributeAllowed(FilterFormatInterface $format, string $tag, string $attribute): ?bool {
    $restrictions = $format->getHtmlRestrictions();
    if ($restrictions === FALSE) {
      // Format has no restrictions.
      return TRUE;
    }

    // The tag itself is not allowed.
    if (!$this->isTagAllowed($format, $tag)) {
      return FALSE;
    }

    // First priority are the global allowed attributes, so recursively check
    // those but continue if we got no result.
    if ($tag !== '*' && !empty($restrictions['allowed']['*'])) {
      $result = $this->isAttributeAllowed($format, '*', $attribute);
      if (isset($result)) {
        return $result;
      }
    }

    if (isset($restrictions['allowed'][$tag]) && is_array($restrictions['allowed'][$tag])) {
      // Then check if the specific tag attribute is allowed.
      if (isset($restrictions['allowed'][$tag][$attribute])) {
        return !empty($restrictions['allowed'][$tag][$attribute]);
      }

      foreach ($restrictions['allowed'][$tag] as $tag_attribute => $attribute_allowed) {
        // Is this a wildcard attribute name, if so, treat it differently.
        if (strpos($tag_attribute, '*') !== FALSE) {
          $attribute_substring = str_replace('*', '', $tag_attribute);
          if (strpos($attribute, $attribute_substring) === 0) {
            return !empty($attribute_allowed);
          }
        }
      }
    }

    return $tag === '*' ? NULL : FALSE;
  }

}
