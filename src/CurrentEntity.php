<?php

namespace Drupal\helper;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Provides helper for working with current entities.
 */
class CurrentEntity {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * CurrentEntity constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route match.
   */
  public function __construct(ModuleHandlerInterface $module_handler, RouteMatchInterface $route_match) {
    $this->moduleHandler = $module_handler;
    $this->routeMatch = $route_match;
  }

  /**
   * Gets the entity from a route match.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface|null $route_match
   *   The route match to use, otherwise uses the current route match.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The current page entity, or NULL if this is not an entity route.
   *
   * @see metatag_get_route_entity()
   */
  public function fromRouteMatch(?RouteMatchInterface $route_match = NULL): ?EntityInterface {
    $route_match ??= $this->routeMatch;
    $route_name = $route_match->getRouteName();

    // The route name may be empty.
    if (!isset($route_name)) {
      return NULL;
    }

    $entity = &drupal_static(__FUNCTION__ . ':' . $route_name, FALSE);
    if ($entity === FALSE) {
      switch ($route_name) {
        case 'entity.node.preview':
          $entity = $route_match->getParameter('node_preview');
          break;

        // Look for a canonical entity view page, e.g. node/{nid}, user/{uid}.
        case (bool) preg_match('/^entity\.([^.]+)\./', $route_name, $matches):
          // Look for a layout builder entity override page, e.g. node/{nid}/layout.
        case (bool) preg_match('/^layout_builder\.overrides\.([^.]+)\.view$/', $route_name, $matches):
          // Look for a rest entity view page, e.g. node/{nid}?_format=json.
        case (bool) preg_match('/^rest\.entity\.([^.]+)\./', $route_name, $matches);
          $entity = $route_match->getParameter($matches[1]);
          break;

        default:
          $entity = NULL;
          // Call the metatag module hook for the current entity.
          if ($entities = $this->moduleHandler->invokeAll('metatag_route_entity', [$route_match])) {
            $entity = reset($entities);
          }
          break;
      }

      // Allow modules to alter the current entity.
      $this->moduleHandler->alter('helper_current_entity_from_route_match', $entity, $route_match);

      // Double check we actually have an entity.
      if (!($entity instanceof EntityInterface)) {
        $entity = NULL;
      }
    }

    return $entity;
  }

}
