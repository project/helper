<?php

declare(strict_types=1);

namespace Drupal\helper;

/**
 * Provides helper for working with arrays.
 */
class ArrayHelper {

  /**
   * Checks if an array contains a value.
   *
   * @param array $array
   *   The array.
   * @param mixed $value
   *   The value to search for.
   * @param bool $strict
   *   Passed to array_search() for strict comparison.
   * @param array|null $keys
   *   The keys of the values if found, set by reference.
   *
   * @return bool
   *   TRUE if the array has the value, or FALSE otherwise.
   */
  public static function hasValue(array $array, mixed $value, bool $strict = TRUE, ?array &$keys = NULL): bool {
    $keys = array_keys($array, $value, $strict);
    return !empty($keys);
  }

  /**
   * Replace array value.
   *
   * @param array $array
   *   The array.
   * @param mixed $search
   *   The value(s) to search for.
   * @param mixed $replace
   *   The replacement value(s).
   * @param bool $strict
   *   Passed to array_search() for strict comparison.
   */
  public static function replaceValue(array &$array, mixed $search, mixed $replace, bool $strict = TRUE): void {
    if (static::hasValue($array, $search, $strict, $keys)) {
      $array = array_replace($array, array_fill_keys($keys, $replace));
    }
  }

  /**
   * Replace multiple array values.
   *
   * @param array $array
   *   The array.
   * @param array $replacements
   *   The replacement values, keyed by their original values.
   * @param bool $strict
   *   Passed to array_search() for strict comparison.
   */
  public static function replaceValues(array &$array, array $replacements, bool $strict = TRUE): void {
    foreach ($replacements as $needle => $replacement) {
      static::replaceValue($array, $needle, $replacement, $strict);
    }
  }

  /**
   * Replace array key.
   *
   * @param array $array
   *   The array, modified by reference.
   * @param mixed $search
   *   The key to search for.
   * @param mixed $replace
   *   The replacement key.
   * @param bool $strict
   *   Passed to array_search() for strict comparison.
   */
  public static function replaceKey(array &$array, mixed $search, mixed $replace, bool $strict = TRUE): void {
    $keys = array_keys($array);
    static::replaceValue($keys, $search, $replace, $strict);
    $array = array_combine($keys, $array);
  }

  /**
   * Replace array keys.
   *
   * @param array $array
   *   The array, modified by reference.
   * @param array $replacements
   *   The replacement keys, keyed by their original values.
   * @param bool $strict
   *   Passed to array_search() for strict comparison.
   */
  public static function replaceKeys(array &$array, array $replacements, bool $strict = TRUE): void {
    foreach ($replacements as $needle => $replacement) {
      static::replaceKey($array, $needle, $replacement, $strict);
    }
  }

  /**
   * Add a value to an array if it is not already present.
   *
   * @param array $array
   *   The array, modified by reference.
   * @param mixed $value
   *   The value to add.
   * @param mixed|null $key
   *   The key to use for the value if it is needed.
   * @param bool $strict
   *   Passed to array_search() for strict comparison.
   *
   * @return bool
   *   TRUE if the value was added, or FALSE otherwise.
   */
  public static function addUniqueValue(array &$array, mixed $value, mixed $key = NULL, bool $strict = TRUE): bool {
    if (isset($key)) {
      if (!array_key_exists($key, $array)) {
        $array[$key] = $value;
        return TRUE;
      }
      return FALSE;
    }

    if (!static::hasValue($array, $value, $strict)) {
      $array[] = $value;
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Remove an array's value.
   *
   * @param array $array
   *   The array, modified by reference.
   * @param mixed $value
   *   The value to search for.
   * @param bool $strict
   *   Passed to array_search() for strict comparison.
   *
   * @return bool
   *   TRUE if the item was removed, or FALSE otherwise.
   */
  public static function removeValue(array &$array, mixed $value, bool $strict = FALSE): bool {
    if (static::hasValue($array, $value, $strict, $keys)) {
      $array = array_diff_key($array, array_flip($keys));
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Applies the callback to the keys of the given array.
   *
   * @param array $array
   *   An array to run through the callback function.
   * @param callable $callback
   *   Callback function to run for each key in the array.
   *
   * @return array
   *   Returns an array containing all the elements of $array after applying
   *   the callback function to each key.
   */
  public static function mapKeys(array $array, callable $callback): array {
    return array_combine(array_map($callback, array_combine(array_keys($array), array_keys($array))), $array);
  }

  /**
   * Filters keys of an array using a callback function.
   *
   * @param array $array
   *   The array to iterate over.
   * @param callable|null $callback
   *   The callback function to use. If no callback is supplied, all keys of
   *   array equal to FALSE will be removed.
   *
   * @return array
   *   Returns the filtered array.
   */
  public static function filterKeys(array $array, ?callable $callback = NULL): array {
    return array_intersect_key($array, array_flip(array_filter(array_keys($array), $callback)));
  }

  /**
   * Split an array into chunks more evenly then array_chunk().
   *
   * @param array $array
   *   An array of data to split up.
   * @param int $chunks
   *   The amount of chunks to create.
   * @param bool $preserve_keys
   *   When set to TRUE keys will be preserved. Default is FALSE which will
   *   reindex the chunk numerically.
   *
   * @return array
   *   Returns a multidimensional numerically indexed array, starting with
   *   zero, with each dimension containing size elements.
   *
   * @see http://php.net/manual/en/function.array-chunk.php#75022
   *
   * @throws \DomainException
   */
  public static function chunkEvenly(array $array, int $chunks, bool $preserve_keys = FALSE): array {
    if ($chunks < 1) {
      throw new \DomainException("Cannot call " . __METHOD__ . " with \$chunks being less than one.");
    }

    $size = count($array);
    if (!$size) {
      return [];
    }
    if ($chunks > $size) {
      $chunks = $size;
    }

    $chunk_size = (int) floor($size / $chunks);
    $remainder = $size % $chunks;
    $result = [];
    $current_index = 0;
    for ($chunk = 0; $chunk < $chunks; $chunk++) {
      $increment = ($chunk < $remainder) ? $chunk_size + 1 : $chunk_size;
      $result[] = array_slice($array, $current_index, $increment, $preserve_keys);
      $current_index += $increment;
    }
    return $result;
  }

}
