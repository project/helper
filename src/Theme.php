<?php

namespace Drupal\helper;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Extension\ThemeInstallerInterface;
use Drupal\Core\Theme\ThemeManagerInterface;

/**
 * Helper service for working with themes.
 */
class Theme {

  /**
   * Theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected ThemeHandlerInterface $themeHandler;

  /**
   * Theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected ThemeManagerInterface $themeManager;

  /**
   * Theme installer.
   *
   * @var \Drupal\Core\Extension\ThemeInstallerInterface
   */
  protected ThemeInstallerInterface $themeInstaller;

  /**
   * Configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Constructs a Theme helper.
   *
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   Theme handler.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   *   Theme manager.
   * @param \Drupal\Core\Extension\ThemeInstallerInterface $theme_installer
   *   Theme installer.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory.
   */
  public function __construct(ThemeHandlerInterface $theme_handler, ThemeManagerInterface $theme_manager, ThemeInstallerInterface $theme_installer, ConfigFactoryInterface $config_factory) {
    $this->themeHandler = $theme_handler;
    $this->themeManager = $theme_manager;
    $this->themeInstaller = $theme_installer;
    $this->configFactory = $config_factory;
  }

  /**
   * Checks if a theme is the active theme or one of the active base themes.
   *
   * @param string $theme_name
   *   Theme name.
   * @param bool $check_base_themes
   *   TRUE to check if the theme is in the base themes of the active theme.
   *
   * @return bool
   *   TRUE if the theme is active, or FALSE otherwise.
   */
  public function isActive(string $theme_name, bool $check_base_themes = FALSE): bool {
    // Check default frontend theme.
    $active_theme = $this->themeManager->getActiveTheme()->getName();
    if ($active_theme === $theme_name) {
      return TRUE;
    }

    // Check any base themes to see if they match.
    if ($check_base_themes && $this->isBaseTheme($theme_name, $active_theme)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Checks if a theme is the default theme or one of the default base themes.
   *
   * @param string $theme_name
   *   Theme name.
   * @param bool $check_base_themes
   *   TRUE to check if the theme is in the base themes of the default theme.
   *
   * @return bool
   *   TRUE if the theme is default, or FALSE otherwise.
   */
  public function isDefault(string $theme_name, bool $check_base_themes = FALSE): bool {
    // Check default frontend theme.
    $default_theme = $this->themeHandler->getDefault();
    if ($default_theme === $theme_name) {
      return TRUE;
    }

    // Check any base themes to see if they match.
    // Default theme may return NULL instead of a string in tests.
    if ($check_base_themes && $default_theme && $this->isBaseTheme($theme_name, $default_theme)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Checks if a theme is a base theme of a sub-theme.
   *
   * @param string $base_theme
   *   The theme that is potentially a base theme.
   * @param string $theme_name
   *   The sub theme name to check for base themes.
   *
   * @return bool
   *   TRUE if the theme is a base theme or FALSE otherwise.
   */
  public function isBaseTheme(string $base_theme, string $theme_name): bool {
    // Check any base themes to see if they match.
    if ($base_themes = $this->themeHandler->getBaseThemes($this->themeHandler->listInfo(), $theme_name)) {
      return array_key_exists($base_theme, $base_themes);
    }
    return FALSE;
  }

  /**
   * Sets the default theme.
   *
   * @param string $theme_name
   *   Theme name.
   *
   * @return bool
   *   TRUE if the default theme was updated, or FALSE if the requested theme
   *   was already the default.
   */
  public function setDefault(string $theme_name): bool {
    if (!$this->themeHandler->themeExists($theme_name)) {
      $this->themeInstaller->install([$theme_name]);
    }
    if (!$this->isDefault($theme_name)) {
      $this->configFactory->getEditable('system.theme')->set('default', $theme_name)->save();
      $this->themeManager->resetActiveTheme();
      return TRUE;
    }
    return FALSE;
  }

}
