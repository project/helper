<?php

namespace Drupal\helper;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\Config as CoreConfig;
use Drupal\Core\Config\ConfigDirectoryNotDefinedException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Site\Settings;
use Psr\Log\LoggerInterface;

/**
 * Provides helper for working with configuration.
 */
class Config {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The configuration manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleList;

  /**
   * Flag if existing active config should be updated when importing.
   *
   * @var bool
   */
  protected bool $updateExistingConfig = TRUE;

  /**
   * Config constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The configuration manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_list
   *   The module extension list.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ConfigManagerInterface $config_manager, LoggerInterface $logger, FileSystemInterface $file_system, ModuleExtensionList $module_list) {
    $this->configFactory = $config_factory;
    $this->configManager = $config_manager;
    $this->logger = $logger;
    $this->fileSystem = $file_system;
    $this->moduleList = $module_list;
  }

  /**
   * Sets a flag to update existing configuration when importing the same file.
   *
   * @param bool $update
   *   TRUE to allow updates to existing configuration on file import, or FALSE
   *   to ignore imports when existing configuration already exists.
   */
  public function updateExisting(bool $update = TRUE): self {
    $this->updateExistingConfig = $update;
    return $this;
  }

  /**
   * Skip config imports if existing configuration already exists.
   */
  public function skipExisting(): self {
    return $this->updateExisting(FALSE);
  }

  /**
   * Determines if configuration is currently exported.
   *
   * @param string $config_name
   *   The specific configuration to look for, defaults to system.site that
   *   should be required to have a Drupal site with exported config.
   *
   * @return bool
   *   TRUE if configuration file is exported or FALSE if not.
   */
  public function isExported(string $config_name = 'system.site'): bool {
    try {
      $config_directory = $this->getSyncDirectory();
      // @todo This does not currently handle more advanced configuration storage/collections like config_split.
      return $config_directory && is_file(DRUPAL_ROOT . '/' . $config_directory . '/' . $config_name . '.yml');
    }
    catch (ConfigDirectoryNotDefinedException $exception) {
      return FALSE;
    }
  }

  /**
   * Gets the config sync directory.
   *
   * @return string
   *   The configuration sync directory.
   */
  public function getSyncDirectory(): string {
    $directory = Settings::get('config_sync_directory', FALSE);
    if ($directory === FALSE) {
      throw new ConfigDirectoryNotDefinedException('The config sync directory is not defined in $settings["config_sync_directory"]');
    }
    assert(is_string($directory));
    return $directory;
  }

  /**
   * Checks if a configuration exists or not.
   *
   * This works for both simple config and configuration entities.
   *
   * @param string $config_name
   *   The configuration name.
   *
   * @return bool
   *   TRUE if the configuration exists.
   */
  public function exists(string $config_name): bool {
    $config = $this->load($config_name);
    return $config && !$config->isNew();
  }

  /**
   * Load a configuration.
   *
   * This works for both simple config and configuration entities.
   *
   * @param string $config_name
   *   The configuration name.
   *
   * @return \Drupal\Core\Config\Entity\ConfigEntityInterface|\Drupal\Core\Config\Config|null
   *   The configuration if it could be loaded or NULL if it could not be
   *   loaded (or created if $created is TRUE).
   */
  public function load(string $config_name): ConfigEntityInterface|CoreConfig|null {
    // Check if this is a configuration entity first.
    if ($entity_storage = $this->getEntityStorage($config_name)) {
      $entity_id = $this->getEntityId($entity_storage, $config_name);
      $entity = $entity_storage->load($entity_id);
      assert($entity instanceof ConfigEntityInterface);
      return $entity;
    }

    $config = $this->configFactory->getEditable($config_name);
    if ($config->isNew()) {
      return NULL;
    }
    return $config;
  }

  /**
   * Creates a configuration.
   *
   * This works for both simple config and configuration entities.
   *
   * @param string $config_name
   *   The configuration name.
   * @param array $data
   *   The configuration data.
   *
   * @return \Drupal\Core\Config\Entity\ConfigEntityInterface|\Drupal\Core\Config\Config
   *   The configuration object.
   */
  public function create(string $config_name, array $data): ConfigEntityInterface|CoreConfig {
    // Check if this is a configuration entity first.
    if ($entity_storage = $this->getEntityStorage($config_name)) {
      $entity_type = $entity_storage->getEntityType();
      $entity_id = $this->getEntityId($entity_storage, $config_name);
      $entity_id_key = $entity_type->getKey('id');
      $data += [
        $entity_id_key => $entity_id,
      ];

      $entity = $entity_storage->create($data);

      // If this is an existing configuration entity, set the UUID to the
      // existing entity's UUID and enforce that this is not a new entity.
      if ($existing = $entity_storage->load($entity_id)) {
        $entity->enforceIsNew(FALSE);
        $entity->set('uuid', $existing->uuid());
      }

      assert($entity instanceof ConfigEntityInterface);
      return $entity;
    }

    $config = $this->configFactory->getEditable($config_name);
    $config->setData($data);
    return $config;
  }

  /**
   * Get the config data.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface|\Drupal\Core\Config\Config $config
   *   The configuration object.
   *
   * @return array
   *   The configuration data.
   */
  public function getData(ConfigEntityInterface|CoreConfig $config): array {
    return $config instanceof ConfigEntityInterface ? $config->toArray() : $config->getRawData();
  }

  /**
   * Import a single configuration file.
   *
   * @param string $uri
   *   The path to the file.
   * @param string $contents
   *   Optional manual contents of the file.
   *
   * @throws \RuntimeException
   *   If unable to decode YAML from file.
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In unable to save the config.
   */
  public function importFile($uri, $contents = NULL): void {
    if (!isset($contents) && (!$contents = @file_get_contents($uri))) {
      throw new \RuntimeException("Unable to read file $uri.");
    }

    $data = Yaml::decode($contents);
    if (!is_array($data)) {
      throw new \RuntimeException("Unable to decode YAML from $uri.");
    }

    $config_name = basename($uri, '.yml');
    $config = $this->create($config_name, $data);
    if (!$this->updateExistingConfig && !$config->isNew()) {
      $this->logger->notice('Skipping import of @uri since the configuration already exists.', ['@uri' => $uri]);
      return;
    }

    $id = $config instanceof ConfigEntityInterface ? $config->id() : $config_name;
    $this->logger->notice('Importing @uri to @type @name.', ['@uri' => $uri, '@type' => $config::class, '@name' => $id]);
    $config->save();
  }

  /**
   * Import a directory containing configuration files.
   *
   * @param string $directory
   *   The path to the directory.
   * @param array $options
   *   An array of options to pass to file_scan_directory().
   *
   * @throws \RuntimeException
   *   If unable to decode YAML from file.
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In unable to save the config.
   */
  public function importDirectory(string $directory, array $options = []): void {
    $files = $this->fileSystem->scanDirectory($directory, '/^.*\.yml$/', $options);
    foreach ($files as $file) {
      $this->importFile($file->uri);
    }
  }

  /**
   * Import a module's directory containing configuration files.
   *
   * @param string $module
   *   The module name.
   * @param string $directory
   *   The directory name inside the module's config directory. Defaults to
   *   'install'.
   * @param array $options
   *   An array of options to pass to file_scan_directory().
   *
   * @throws \RuntimeException
   *   If unable to decode YAML from file.
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In unable to save the config.
   */
  public function importModule(string $module, string $directory = 'install', array $options = []): void {
    $this->importDirectory($this->moduleList->getPath($module) . '/config/' . $directory, $options);
  }

  /**
   * Export a single configuration file.
   *
   * When a directory is provided, this will by default exclude certain keys
   * from the exported file, like uuid and _core. When not providing a specific
   * directory, the file will be saved to the configuration sync directory and
   * no keys will be filtered out from the export.
   *
   * @param string $config_name
   *   The configuration ID.
   * @param string|null $directory
   *   The directory that the configuration file will be written to. If not
   *   provided the configuration sync directory will be used.
   * @param array $options
   *   An optional array of options.
   *
   * @throws \RuntimeException
   *   If the directory is not writeable or if the configuration does not exist.
   */
  public function exportFile(string $config_name, ?string $directory = NULL, array $options = []): void {
    if (!isset($directory)) {
      $directory = $this->getSyncDirectory();
      // UUIDs must be exported in the sync directory.
      $options['uuid'] = TRUE;
      // Do not filter any keys.
      $options['filter keys'] = [];
    }

    if (!is_writable($directory)) {
      throw new \RuntimeException("The directory $directory is not writeable.");
    }

    $config = $this->load($config_name);
    if (!$config) {
      throw new \RuntimeException("The config $config_name does not exist.");
    }

    $data = $this->getData($config);

    $options += [
      'uuid' => FALSE,
      'filter keys' => ['_core'],
    ];

    // Deprecated uuid option support.
    if (!$options['uuid'] && $this->configManager->getEntityTypeIdByName($config_name)) {
      $options['filter keys'][] = 'uuid';
    }

    // Filter out keys.
    if ($options['filter keys']) {
      $data = array_diff_key($data, array_flip($options['filter keys']));
    }

    // Merge in additional information.
    if (!empty($options['merge']) && is_array($options['merge'])) {
      NestedArray::mergeDeep($data, $options['merge']);
    }

    $uri = $directory . '/' . $config_name . '.yml';
    $this->logger->notice('Exporting @uri.', ['@uri' => $uri]);
    $result = file_put_contents($uri, Yaml::encode($data));
    if ($result === FALSE) {
      throw new \RuntimeException("Error encountered writing to $uri.");
    }
  }

  /**
   * Re-export a directory containing configuration files.
   *
   * @param string $directory
   *   The path to the directory.
   * @param array $options
   *   An array of options to pass to ::exportFile().
   *
   * @throws \RuntimeException
   *   If the directory is not writeable or if the configuration does not exist.
   */
  public function reExportDirectory(string $directory, array $options = []): void {
    $files = $this->fileSystem->scanDirectory($directory, '/^.*\.yml$/');
    foreach ($files as $file) {
      $this->exportFile(basename($file->filename, '.yml'), dirname($file->uri), $options);
    }
  }

  /**
   * Import a module's directory containing configuration files.
   *
   * @param string $module
   *   The module name.
   * @param string $directory
   *   The directory name inside the module's config directory. Defaults to
   *   'install'.
   * @param array $options
   *   An array of options to pass to ::exportFile().
   *
   * @throws \RuntimeException
   *   If the directory is not writeable or if the configuration does not exist.
   */
  public function reExportModule(string $module, string $directory = 'install', array $options = []): void {
    $this->reExportDirectory($this->moduleList->getPath($module) . '/config/' . $directory, $options);
  }

  /**
   * Gets the configuration storage.
   *
   * @param string $config_name
   *   The configuration name.
   *
   * @return \Drupal\Core\Config\Entity\ConfigEntityStorageInterface|null
   *   The configuration entity type storage if available.
   */
  protected function getEntityStorage(string $config_name): ?ConfigEntityStorageInterface {
    // Check if this is a configuration entity first.
    if ($entity_type_id = $this->configManager->getEntityTypeIdByName($config_name)) {
      $storage = $this->configManager->getEntityTypeManager()->getStorage($entity_type_id);
      assert($storage instanceof ConfigEntityStorageInterface);
      return $storage;
    }
    return NULL;
  }

  /**
   * Get the entity ID for a configuration object.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $storage
   *   The configuration entity storage.
   * @param string $config_name
   *   The configuration name.
   *
   * @return string
   *   The entity ID.
   */
  protected function getEntityId(ConfigEntityStorageInterface $storage, string $config_name) {
    // getIDFromConfigName adds a dot but getConfigPrefix has a dot already.
    $entity_type = $storage->getEntityType();
    assert($entity_type instanceof ConfigEntityTypeInterface);
    return $storage::getIDFromConfigName($config_name, $entity_type->getConfigPrefix());
  }

}
