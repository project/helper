<?php

declare(strict_types=1);

namespace Drupal\helper\Form;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for implementing configuration forms validatable by constraints.
 *
 * @link https://www.drupal.org/project/drupal/issues/3364506
 */
abstract class ValidatableConfigFormBase extends ConfigFormBase {

  /**
   * Constructs a ValidatableConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, protected TypedConfigManagerInterface $typedConfigManager) {
    if (class_exists('Drupal\Core\Form\ValidatableConfigFormBase')) {
      @trigger_error("Drupal\Core\Form\ValidatableConfigFormBase should be used instead of " . __CLASS__, E_USER_DEPRECATED);
    }
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($this->getEditableConfigNames() as $config_name) {
      $config = static::mapFormValuesToConfig($form_state, $this->config($config_name));
      $typed_config = $this->typedConfigManager->createFromNameAndData($config_name, $config->getRawData());

      $violations = $typed_config->validate();
      foreach ($violations as $violation) {
        $form_state->setErrorByName(static::mapConfigPropertyPathToFormElementName($config_name, $violation->getPropertyPath()), $violation->getMessage());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($this->getEditableConfigNames() as $config_name) {
      $config = static::mapFormValuesToConfig($form_state, $this->config($config_name));
      $config->save();
    }
    parent::submitForm($form, $form_state);
  }

  /**
   * Maps the form values from form state onto the given editable Config.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\Core\Config\Config $config
   *   The configuration being edited.
   *
   * @return \Drupal\Core\Config\Config
   *   The updated configuration object.
   */
  abstract protected static function mapFormValuesToConfig(FormStateInterface $form_state, Config $config): Config;

  /**
   * Maps the given violation constraint property path to a form name.
   *
   * @param string $config_name
   *   The name of the Config whose property path triggered a validation error.
   * @param string $property_path
   *   The property path that triggered a validation error.
   *
   * @return string
   *   The corresponding form name.
   */
  protected static function mapConfigPropertyPathToFormElementName(string $config_name, string $property_path) : string {
    // A default implementation, which is suitable when the configuration is
    // mapped 1:1 to form elements.
    return str_replace('.', '][', $property_path);
  }

}
