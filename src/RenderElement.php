<?php

namespace Drupal\helper;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Markup;

/**
 * Utility helpers for working with render elements.
 */
class RenderElement {

  /**
   * Add a wrapper around a render element.
   *
   * @param array $element
   *   The render element.
   * @param string|\Drupal\Component\Render\MarkupInterface $prefix
   *   The prefix to add to the element.
   * @param string|\Drupal\Component\Render\MarkupInterface $suffix
   *   The suffix to add to the element.
   * @param bool $inside
   *   If the wrapper should be inside the existing prefix and suffix.
   */
  public static function wrap(array &$element, string|MarkupInterface $prefix, string|MarkupInterface $suffix, bool $inside = FALSE): void {
    if ($prefix !== '') {
      $element['#prefix'] = Markup::create($inside ? ($element['#prefix'] ?? '') . $prefix : $prefix . ($element['#prefix'] ?? ''));
    }
    if ($suffix !== '') {
      $element['#suffix'] = Markup::create($inside ? $suffix . ($element['#suffix'] ?? '') : ($element['#suffix'] ?? '') . $suffix);
    }
  }

  /**
   * Apply a callback to the render element children.
   *
   * @param array $element
   *   The render element.
   * @param callable $callback
   *   The callback to apply.
   * @param mixed ...$args
   *   The additional parameters to provide to the callback.
   */
  public static function applyToChildren(array &$element, callable $callback, mixed ...$args): void {
    foreach (Element::children($element) as $key) {
      $callback($element[$key], ...$args);
    }
  }

  /**
   * Attach a library to the render element.
   *
   * @link https://www.drupal.org/docs/creating-modules/adding-assets-css-js-to-a-drupal-module-via-librariesyml#render-array
   *
   * @param array $element
   *   The render element.
   * @param string $library
   *   The library, like 'your_module/library_name'.
   */
  public static function attachLibrary(array &$element, string $library): void {
    $element['#attached']['library'][] = $library;
  }

  /**
   * Attach a single drupalSettings key/value.
   *
   * @link https://www.drupal.org/docs/creating-modules/adding-assets-css-js-to-a-drupal-module-via-librariesyml#render-array
   *
   * @param array $element
   *   The render element.
   * @param string $key
   *   The drupalSettings key.
   * @param mixed $value
   *   The drupalSettings value.
   *
   * @link https://www.drupal.org/node/2274843#configurable
   */
  public static function attachDrupalSetting(array &$element, string $key, mixed $value): void {
    $element['#attached']['drupalSettings'][$key] = $value;
  }

  /**
   * Attach multiple drupalSettings.
   *
   * @link https://www.drupal.org/docs/creating-modules/adding-assets-css-js-to-a-drupal-module-via-librariesyml#render-array
   *
   * @param array $element
   *   The render element.
   * @param array $settings
   *   The drupalSettings key/value array.
   *
   * @link https://www.drupal.org/node/2274843#configurable
   */
  public static function attachDrupalSettings(array &$element, array $settings): void {
    $element['#attached']['drupalSettings'] += $settings;
  }

  /**
   * Attach a HTML header.
   *
   * @param array $element
   *   The render element.
   * @param string $name
   *   The header name, 'status' is treated specially and used as http status
   *   code.
   * @param string $value
   *   The header value.
   * @param bool $replace
   *   (optional) Whether to replace a current value with the new one, or add
   *   it to the others. If the value is not replaced, it will be appended,
   *   resulting in a header like this: 'Header: value1,value2'.
   *
   * @see \Drupal\Core\Render\HtmlResponseAttachmentsProcessor::setHeaders
   */
  public static function attachHeader(array &$element, string $name, string $value, bool $replace = FALSE): void {
    $element['#attached']['http_header'][] = [$name, $value, $replace];
  }

  /**
   * Attach a feed.
   *
   * @param array $element
   *   The render element.
   * @param string $href
   *   The feed href.
   * @param string|null $title
   *   The feed title.
   *
   * @see \Drupal\Core\Render\HtmlResponseAttachmentsProcessor::processFeed
   */
  public static function attachFeed(array &$element, string $href, string $title = NULL): void {
    $element['#attached']['feed'][] = [$href, $title];
  }

  /**
   * Attach a head link.
   *
   * @param array $element
   *   The render element.
   * @param string $href
   *   The link href.
   * @param string $rel
   *   The link rel.
   * @param array $attributes
   *   Optionally more link attributes.
   * @param bool $shouldAddHeader
   *   A boolean specifying whether the link should also be a Link: HTTP header.
   *
   * @see \Drupal\Core\Render\HtmlResponseAttachmentsProcessor::processHtmlHeadLink
   * @link https://developer.mozilla.org/en-US/docs/Web/HTML/Element/link
   */
  public static function attachHeadLink(array &$element, string $href, string $rel, array $attributes = [], bool $shouldAddHeader = FALSE): void {
    $attributes += [
      'href' => $href,
      'rel' => $rel,
    ];
    $element['#attached']['html_head_link'][] = [$attributes, $shouldAddHeader];
  }

  /**
   * Attach an inline CSS style.
   *
   * @param array $element
   *   The render element.
   * @param string $style
   *   The inline CSS style value.
   */
  public static function attachStyle(array &$element, string $style): void {
    if (!empty($element['#attributes']['style'])) {
      $element['#attributes']['style'] = rtrim($element['#attributes']['style'], '; ') . '; ' . $style;
    }
    else {
      $element['#attributes']['style'] = $style;
    }
  }

  /**
   * Change the access of a render element.
   *
   * @param array $element
   *   The render element.
   * @param bool|\Drupal\Core\Access\AccessResultInterface $access
   *   The access to merge.
   * @param bool $and
   *   If the access should be an andIf if TRUE or orIf if FALSE.
   */
  public static function mergeAccess(array &$element, bool|AccessResultInterface $access, bool $and = TRUE): void {
    if (isset($element['#access_callback'])) {
      throw new \RuntimeException("Cannot set #access on a render element that already has #access_callback.");
    }

    // If access hasn't been set yet, shortcut to assignment and return.
    if (!isset($element['#access'])) {
      $element['#access'] = $access;
      return;
    }

    $current_access = $element['#access'];
    if (is_bool($current_access) && is_bool($access)) {
      $element['#access'] = $and ? ($current_access && $access) : ($current_access || $access);
    }
    else {
      // Core treats everything that is not FALSE as TRUE.
      // @see \Drupal\Core\Render\Renderer::doRender
      if (is_bool($access)) {
        $access = AccessResult::allowedIf($access !== FALSE);
      }
      if (is_bool($current_access)) {
        $current_access = AccessResult::allowedIf($current_access !== FALSE);
      }
      $element['#access'] = $and ? $current_access->andIf($access) : $current_access->orIf($access);
    }
  }

}
