<?php

namespace Drupal\helper;

use Drupal\Component\Plugin\CategorizingPluginManagerInterface;
use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Plugin\FilteredPluginManagerInterface;

/**
 * Provides various utility helpers for plugins.
 */
class Plugin {

  /**
   * Most plugins use a 'label' key. Some do not.
   */
  protected const KNOWN_LABEL_KEYS = [
    'block' => 'admin_label',
    'filter' => 'title',
  ];

  /**
   * Get the plugin manager.
   *
   * @param string $plugin_type
   *   The plugin type.
   *
   * @return \Drupal\Component\Plugin\PluginManagerInterface
   *   The plugin manager.
   */
  public static function getManager(string $plugin_type): PluginManagerInterface {
    $plugin_manager = \Drupal::service('plugin.manager.' . $plugin_type);
    assert($plugin_manager instanceof PluginManagerInterface);
    return $plugin_manager;
  }

  /**
   * Call a plugin callback and return its result.
   *
   * @param string $plugin_type
   *   The plugin type.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $callback
   *   The method to call on the plugin class.
   * @param mixed ...$args
   *   The arguments to pass to the plugin callback.
   *
   * @return mixed
   *   The return value from the plugin method.
   */
  public static function invoke(string $plugin_type, string $plugin_id, array $configuration, string $callback, ...$args) {
    return static::getManager($plugin_type)->createInstance($plugin_id, $configuration)->$callback(...$args);
  }

  /**
   * Get an option list of allowed plugins.
   *
   * @param string $plugin_type
   *   The plugin type.
   * @param array|string|null $definitions
   *   The definitions to convert to options. If provided as a string, it will
   *   be passed to getFilteredDefinitions() as the first $consumer argument.
   *   If not provided they will be fetched from the plugin manager.
   * @param array $options
   *   An optional array of additional options:
   *   - label key (string): What key to use in the definition as the label.
   *   - fallback (boolean): If the fallback plugin should be included.
   *   - contexts (array): Parameter to pass to getFilteredDefinitions().
   *   - extra (array): Parameter to pass to getFilteredDefinitions().
   *
   * @return array
   *   The list of plugins ready for a form element #options.
   */
  public static function getOptions(string $plugin_type, array|string|null $definitions = NULL, array $options = []) {
    $plugin_manager = static::getManager($plugin_type);

    $options += [
      // Provide some support for some of the non-standard label keys.
      'label key' => static::KNOWN_LABEL_KEYS[$plugin_type] ?? 'label',
      // Do not include the fallback plugin ID option by default.
      'fallback' => FALSE,
      // Additional options to pass to getFilteredDefinitions().
      'contexts' => NULL,
      'extra' => [],
    ];

    if (is_string($definitions)) {
      if ($plugin_manager instanceof FilteredPluginManagerInterface) {
        $definitions = $plugin_manager->getFilteredDefinitions($definitions, $options['contexts'] ?? [], $options['extra'] ?? []);
      }
      else {
        throw new \RuntimeException("Cannot use string definition because " . $plugin_manager::class . " does not implement FilteredPluginManagerInterface.");
      }
    }
    else {
      $definitions ??= $plugin_manager->getDefinitions();
    }

    // If this plugin manager supports a fallback plugin, we will want to
    // remove it from the options.
    if (!$options['fallback'] && $plugin_manager instanceof FallbackPluginManagerInterface) {
      $options['fallback'] = $plugin_manager->getFallbackPluginId('fallback');
    }

    // Create a temporary function to convert definitions to options.
    $map_definitions_to_options = static function (array $definitions) use ($options) {
      $result = [];
      foreach ($definitions as $id => $definition) {
        // If the plugin ID matches the fallback, skip it.
        if ($id === $options['fallback']) {
          continue;
        }

        // Some plugin definitions are classes, but most are simple arrays.
        $result[$id] = (string) (is_array($definition) ? $definition[$options['label key']] : $definition->get($options['label key']));
      }

      // Remove any empty keys and sort naturally.
      $result = ArrayHelper::filterKeys($result);
      natcasesort($result);
      return $result;
    };

    if ($plugin_manager instanceof CategorizingPluginManagerInterface) {
      $definitions = $plugin_manager->getGroupedDefinitions($definitions);
      $result = array_map(static function (array $category_definitions) use ($map_definitions_to_options) {
        return $map_definitions_to_options($category_definitions);
      }, $definitions);

      // Remove any empty groups.
      $result = ArrayHelper::filterKeys($result);

      // If there is only one category, just return the items in the category.
      if (count($result) === 1) {
        return reset($result);
      }

      // Ensure categories have the first letter capitalized.
      $result = ArrayHelper::mapKeys($result, static function ($category) {
        return Unicode::ucfirst($category);
      });

      // Sort naturally.
      ksort($result, SORT_NATURAL | SORT_FLAG_CASE);

      return $result;
    }
    else {
      return $map_definitions_to_options($definitions);
    }
  }

}
