<?php

namespace Drupal\helper\File;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\UseCacheBackendTrait;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

/**
 * Helper for working with JSON files.
 */
class JsonFile extends \SplFileInfo {

  use UseCacheBackendTrait;

  /**
   * Constructs a JsonFile.
   *
   * @param string $filename
   *   The filename/URI to the file.
   * @param \Drupal\Core\Cache\CacheBackendInterface|bool $cache
   *   An optional cache backend or FALSE to disable file contents caching.
   */
  public function __construct(string $filename, CacheBackendInterface|bool $cache = NULL) {
    parent::__construct($filename);

    // Having issues injecting this via services, so optional parameter.
    if ($cache instanceof CacheBackendInterface) {
      $this->cacheBackend = $cache;
    }
    elseif (\Drupal::hasService('cache.helper_file')) {
      // This could be invoked before Drupal is installed.
      $this->cacheBackend = \Drupal::service('cache.helper_file');
      $this->useCaches = is_bool($cache) ? $cache : TRUE;
    }
  }

  /**
   * Gets contents of a JSON file.
   *
   * @return array
   *   The decoded JSON contents.
   *
   * @throws \RuntimeException
   * @throws \JsonException
   */
  public function readData(): array {
    $this->validateReadable();

    $uri = $this->getPathname();
    if ($cache = $this->cacheGet($uri)) {
      // Compare cache created to last modified item to ensure current results.
      if ($this->getMTime() < $cache->created) {
        return $cache->data;
      }
    }

    $contents = file_get_contents($uri);
    if ($contents === FALSE) {
      throw new AccessDeniedException($uri);
    }
    $contents = json_decode($contents, TRUE, 512, JSON_THROW_ON_ERROR);
    $this->cacheSet($uri, $contents);
    return $contents;
  }

  /**
   * Writes contents to a JSON file.
   *
   * @param mixed $data
   *   The contents to write to a file. If not a string, this will JSON encode
   *   the data before writing to the file.
   *
   * @throws \RuntimeException
   * @throws \JsonException
   */
  public function writeData(mixed $data): void {
    $this->validateWritable();

    $uri = $this->getPathname();
    if (!is_string($data)) {
      $data = json_encode($data, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT);
    }

    if (file_put_contents($uri, $data) === FALSE) {
      throw new \RuntimeException("The file {$uri} could not be written to.");
    }

    // Reset the static cache.
    $this->cacheBackend->invalidate($uri);
  }

  /**
   * Validates that the directory is writable or and composer.json is writable.
   *
   * @throws \RuntimeException
   */
  public function validateWritable(): void {
    if (($this->isFile() && !$this->isWritable()) || (is_dir($this->getPath()) && !is_writable($this->getPath()))) {
      throw new \RuntimeException("The file {$this->getPathname()} does not exist or is not writable.");
    }
  }

  /**
   * Validates that the directory is writable or and composer.json is writable.
   *
   * @throws \RuntimeException
   */
  public function validateReadable(): void {
    if (!$this->isFile() || !$this->isReadable()) {
      throw new \RuntimeException("The file {$this->getPathname()} does not exist or is not readable.");
    }
  }

}
