<?php

namespace Drupal\helper\File;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\helper\Utility;
use DrupalEnvironment\Environment;
use DrupalFinder\DrupalFinder;

/**
 * Helper for working with Drupal's Composer file.
 */
class ComposerFile extends JsonFile {

  /**
   * The DrupalFinder.
   *
   * @var \DrupalFinder\DrupalFinder
   */
  public $drupalFinder;

  /**
   * {@inheritdoc}
   */
  public function __construct(string $filename = NULL, CacheBackendInterface|bool $cache = NULL) {
    $this->drupalFinder = new DrupalFinder();
    if (!$this->drupalFinder->locateRoot(DRUPAL_ROOT)) {
      throw new \RuntimeException("Unable to find Composer root directory from " . DRUPAL_ROOT);
    }

    // Default to the Drupal composer root directory.
    $filename ??= $this->drupalFinder->getComposerRoot();

    // If the URI is a directory, append the default composer filename.
    if (is_dir($filename)) {
      $filename .= '/' . Environment::getcomposerFilename();
    }

    // Replace any URIs that start with vendor/ with the actual directory.
    if (str_starts_with($filename, 'vendor/')) {
      $filename = str_replace('vendor/', $this->drupalFinder->getVendorDir() . '/', $filename);
    }

    parent::__construct($filename, $cache);
  }

  /**
   * Runs a composer command.
   *
   * Note: USE THIS AT YOUR OWN RISK. YOU ARE RESPONSIBLE FOR ESCAPING THE
   * INPUT TO THE $command PARAMETER.
   *
   * @param string $command
   *   The composer command to run, without the "composer" part.
   * @param mixed $result_code
   *   The result code from running the command, modified by reference.
   *
   * @throws \RuntimeException
   */
  public function exec(string $command, mixed &$result_code = NULL): void {
    if ($this->getFilename() === Environment::getComposerFilename()) {
      $this->validateWritable();
    }
    else {
      throw new \RuntimeException("Unable to run composer command $command without a composer.json in {$this->getPath()}");
    }

    Utility::execCommand('composer', "--working-dir={$this->getPath()} $command", $result_code);
  }

}
