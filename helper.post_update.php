<?php

/**
 * @file
 * Post update functions for Helper.
 */

/**
 * Clear caches due to InstallHelper service change.
 */
function helper_post_update_install_profile_service_change() {
  // Empty post-update hook.
}

/**
 * Clear caches due to Config service change.
 */
function helper_post_update_config_service_change_added_extension_list() {
  // Empty post-update hook.
}

/**
 * Clear caches due to new CurrentEntity service.
 */
function helper_post_update_current_entity_service_added() {
  // Empty post-update hook.
}

/**
 * Clear caches due to new Theme helper service.
 */
function helper_post_update_theme_service_added() {
  // Empty post-update hook.
}

/**
 * Clear caches due to new cache.helper_file service.
 */
function helper_post_update_service_cache_helper_added() {
  // Empty post-update hook.
}
