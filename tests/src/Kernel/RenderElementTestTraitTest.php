<?php

declare(strict_types=1);

namespace Drupal\Tests\helper\Kernel;

use Drupal\Core\Render\Markup;
use Drupal\helper\Tests\RenderElementTestTrait;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the render element test trait.
 *
 * @coversDefaultClass \Drupal\helper\Tests\RenderElementTestTrait
 * @group helper
 */
class RenderElementTestTraitTest extends KernelTestBase {

  use RenderElementTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'helper',
  ];

  /**
   * Tests the assertRenderElement() method.
   *
   * @covers ::assertRenderElement
   * @covers ::renderElement
   * @covers ::removeWhiteSpace
   * @covers ::removeContentNewlines
   * @covers ::updateFileUrlsToUri
   */
  public function testRendering(): void {
    $element = [
      '#type' => 'html_tag',
      '#tag' => 'a',
      '#attributes' => [
        'href' => \Drupal::service('file_url_generator')->generateString('public://test'),
      ],
      '#value' => Markup::create("  Test \nmarkup  "),
      '#prefix' => "Prefix  <p> ",
      '#suffix' => " </p>\n Suffix ",
    ];
    $this->assertRenderElement("Prefix <p><a href=\"/$this->siteDirectory/files/test\"> Test\nmarkup </a></p>\nSuffix", $element);

    $this->removeContentNewlines();
    $this->assertSame("Prefix <p><a href=\"/$this->siteDirectory/files/test\"> Test markup </a></p> Suffix", $this->getRawContent());

    $this->updateFileUrlsToUri();
    $this->assertSame("Prefix <p><a href=\"public://test\"> Test markup </a></p> Suffix", $this->getRawContent());
  }

}
