<?php

namespace Drupal\Tests\helper\Kernel;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\filter\Entity\FilterFormat;
use Drupal\helper\TextFormat;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests to ensure the TextFormatHelper service works.
 *
 * @group helper
 *
 * @coversDefaultClass \Drupal\helper\TextFormat
 */
class TextFormatTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'filter',
    'helper',
  ];

  /**
   * The TextFormat helper service.
   *
   * @var \Drupal\helper\TextFormat
   */
  protected TextFormat $textFormat;

  /**
   * The filter format storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $filterFormatStorage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->textFormat = $this->container->get('helper.text_format');
    $this->filterFormatStorage = $this->container->get('entity_type.manager')->getStorage('filter_format');

    $filter_format = FilterFormat::create([
      'format' => 'text_format_helper_format_1',
      'name' => $this->randomString(),
      'filters' => [
        'filter_html' => [
          'status' => TRUE,
        ],
      ],
    ]);
    $filter_format->save();

    $filter_format_2 = FilterFormat::create([
      'format' => 'text_format_helper_format_2',
      'name' => $this->randomString(),
      'filters' => [
        'filter_html' => [
          'status' => TRUE,
          'settings' => [
            'allowed_html' => '<em> <strong> <code> <a class foo>',
          ],
        ],
      ],
    ]);
    $filter_format_2->save();
  }

  /**
   * Tests for addTag().
   *
   * @param string $tag
   *   The tag name to add, without enclosing braces. Example: span.
   * @param array $attributes
   *   An array of attributes to add to the tag.
   * @param string $text_format_id
   *   A text format ID.
   *
   * @covers ::addTag
   * @covers ::isTagAllowed
   * @dataProvider providerAddTag
   */
  public function testAddTag(string $tag, array $attributes, string $text_format_id): void {
    /** @var \Drupal\filter\FilterFormatInterface $format */
    $format = $this->filterFormatStorage->load($text_format_id);

    $this->textFormat->addTag($format, $tag, $attributes);

    $this->assertTrue($this->textFormat->isTagAllowed($format, $tag), "Tag $tag is allowed for format $text_format_id.");
    foreach ($attributes as $attribute) {
      $this->assertTrue($this->textFormat->isAttributeAllowed($format, $tag, $attribute), "Tag $tag attribute $attribute is allowed for format $text_format_id.");
      if (str_contains($attribute, '*')) {
        $attribute = str_replace('*', 'wildcard-value', $attribute);
        $this->assertTrue($this->textFormat->isAttributeAllowed($format, $tag, $attribute), "Tag $tag attribute $attribute is allowed for format $text_format_id.");
      }
    }
  }

  /**
   * Provide the data to test the addTag function.
   */
  public function providerAddTag(): array {
    return [
      [
        'a',
        ['data-*', 'class', 'foo'],
        'text_format_helper_format_1',
      ],
      [
        'span',
        ['class'],
        'text_format_helper_format_2',
      ],
    ];
  }

  /**
   * Tests for removeTag().
   *
   * @param string $tag
   *   The tag name to remove, without enclosing braces. Example: span.
   * @param string $text_format_id
   *   A text format ID.
   *
   * @covers ::removeTag
   * @covers ::isTagAllowed
   * @dataProvider providerRemoveTag
   */
  public function testRemoveTag(string $tag, string $text_format_id): void {
    /** @var \Drupal\filter\FilterFormatInterface $format */
    $format = $this->filterFormatStorage->load($text_format_id);

    $this->textFormat->removeTag($format, $tag);

    $this->assertFalse($this->textFormat->isTagAllowed($format, $tag), "Tag $tag is not allowed for format $text_format_id.");
    $this->assertFalse($this->textFormat->isAttributeAllowed($format, $tag, 'class'), "Tag $tag attribute class is not allowed for format $text_format_id.");
  }

  /**
   * Provide the data to test the removeTag function.
   */
  public function providerRemoveTag(): array {
    return [
      [
        'strong',
        'text_format_helper_format_2',
      ],
      [
        'a',
        'text_format_helper_format_1',
      ],
    ];
  }

  /**
   * Tests for removeTagAttributes().
   *
   * @param string $tag
   *   The tag name from which to remove attributes, without enclosing braces.
   * @param array $attributes
   *   An array of attributes to remove from the tag.
   * @param string $text_format_id
   *   A text format ID.
   * @param bool $tag_allowed
   *   If the tag itself is expected to be allowed.
   *
   * @covers ::removeTagAttributes
   * @covers ::isTagAllowed
   * @dataProvider providerRemoveTagAttributes
   */
  public function testRemoveTagAttributes(string $tag, array $attributes, string $text_format_id, bool $tag_allowed = TRUE): void {
    /** @var \Drupal\filter\FilterFormatInterface $format */
    $format = $this->filterFormatStorage->load($text_format_id);

    $this->textFormat->removeTagAttributes($format, $tag, $attributes);

    $this->assertSame($tag_allowed, $this->textFormat->isTagAllowed($format, $tag), "Tag $tag is allowed.");

    foreach ($attributes as $attribute) {
      $this->assertFalse($this->textFormat->isAttributeAllowed($format, $tag, $attribute), "Tag $tag attribute $attribute is not allowed for format $text_format_id.");
      if (str_contains($attribute, '*')) {
        $attribute = str_replace('*', 'wildcard-value', $attribute);
        $this->assertFalse($this->textFormat->isAttributeAllowed($format, $tag, $attribute), "Tag $tag attribute $attribute is allowed for format $text_format_id.");
      }
    }
  }

  /**
   * Provide the data to test the removeTagAttributes function.
   */
  public function providerRemoveTagAttributes(): array {
    return [
      [
        'a',
        ['foo'],
        'text_format_helper_format_1',
      ],
      [
        'a',
        ['class'],
        'text_format_helper_format_1',
      ],
      [
        'span',
        ['class'],
        'text_format_helper_format_2',
        FALSE,
      ],
    ];
  }

}
