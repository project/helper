<?php

declare(strict_types=1);

namespace Drupal\Tests\helper\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the layout builder helper.
 */
abstract class HelperKernelTestBase extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'helper',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['helper']);
  }

  /**
   * Enable or disable a helper.
   *
   * @param string $key
   *   The helper to toggle.
   * @param mixed $value
   *   If the helper should be enabled or not.
   */
  protected function setHelper(string $key, mixed $value): void {
    $this->config('helper.settings')->set('enabled.' . $key, $value)->save();
    drupal_static_reset('_helper_is_enabled');
  }

}
