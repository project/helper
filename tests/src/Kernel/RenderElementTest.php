<?php

namespace Drupal\Tests\helper\Kernel;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Render\Markup;
use Drupal\helper\RenderElement;
use Drupal\helper\Tests\RenderElementTestTrait;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests to ensure the RenderElement helper works.
 *
 * @group helper
 *
 * @coversDefaultClass \Drupal\helper\RenderElement
 */
class RenderElementTest extends KernelTestBase {

  use RenderElementTestTrait;

  /**
   * Tests for wrap()
   *
   * @covers ::wrap
   * @covers ::applyToChildren
   */
  public function testWrap(): void {
    $element = [
      '#markup' => 'Markup',
    ];
    RenderElement::wrap($element, '<span>Wrapper prefix</span>', '<span>Wrapper suffix</span>');
    $this->assertRenderElement('<span>Wrapper prefix</span>Markup<span>Wrapper suffix</span>', $element);

    $element = [
      '#prefix' => Markup::create('<span>Existing prefix</span>'),
      '#markup' => 'Markup',
      '#suffix' => Markup::create('<span>Existing suffix</span>'),
    ];
    RenderElement::wrap($element, Markup::create('<span>Wrapper prefix</span>'), Markup::create('<span>Wrapper suffix</span>'));
    $this->assertRenderElement('<span>Wrapper prefix</span><span>Existing prefix</span>Markup<span>Existing suffix</span><span>Wrapper suffix</span>', $element);

    $element = [
      '#prefix' => Markup::create('<span>Existing prefix</span>'),
      '#markup' => 'Markup',
      '#suffix' => Markup::create('<span>Existing suffix</span>'),
    ];
    RenderElement::wrap($element, Markup::create('<span>Wrapper prefix</span>'), Markup::create('<span>Wrapper suffix</span>'), TRUE);
    $this->assertRenderElement('<span>Existing prefix</span><span>Wrapper prefix</span>Markup<span>Wrapper suffix</span><span>Existing suffix</span>', $element);

    $element = [
      [
        '#markup' => 'First',
      ],
      [
        '#markup' => 'Second',
      ],
    ];
    RenderElement::applyToChildren($element, [RenderElement::class, 'wrap'], '<span>Wrapper prefix</span>', '<span>Wrapper suffix</span>');
    $this->assertRenderElement('<span>Wrapper prefix</span>First<span>Wrapper suffix</span><span>Wrapper prefix</span>Second<span>Wrapper suffix</span>', $element);
  }

  /**
   * Tests for attachStyle()
   *
   * @covers ::attachStyle
   */
  public function testAttachStyle(): void {
    $element = [];
    RenderElement::attachStyle($element, 'color: red');
    $this->assertSame('color: red', $element['#attributes']['style']);

    RenderElement::attachStyle($element, 'display: none; border: none');
    $this->assertSame('color: red; display: none; border: none', $element['#attributes']['style']);
  }

  /**
   * Tests for mergeAccess()
   *
   * @covers ::mergeAccess
   */
  public function testMergeAccess(): void {
    $element = [];
    RenderElement::mergeAccess($element, TRUE);
    $this->assertSame(TRUE, $element['#access']);

    $element = [];
    $access = AccessResult::allowed();
    RenderElement::mergeAccess($element, $access);
    $this->assertSame($access, $element['#access']);

    $element = [
      '#access' => FALSE,
    ];
    RenderElement::mergeAccess($element, TRUE);
    $this->assertSame(FALSE, $element['#access']);

    $element = [
      '#access' => FALSE,
    ];
    RenderElement::mergeAccess($element, TRUE, FALSE);
    $this->assertSame(TRUE, $element['#access']);

    $element = [
      '#access' => FALSE,
    ];
    RenderElement::mergeAccess($element, FALSE);
    $this->assertSame(FALSE, $element['#access']);

    $element = [
      '#access' => FALSE,
    ];
    RenderElement::mergeAccess($element, FALSE, FALSE);
    $this->assertSame(FALSE, $element['#access']);

    $element = [
      '#access' => TRUE,
    ];
    RenderElement::mergeAccess($element, TRUE);
    $this->assertSame(TRUE, $element['#access']);

    $element = [
      '#access' => TRUE,
    ];
    RenderElement::mergeAccess($element, TRUE, FALSE);
    $this->assertSame(TRUE, $element['#access']);

    $element = [
      '#access' => TRUE,
    ];
    RenderElement::mergeAccess($element, FALSE);
    $this->assertSame(FALSE, $element['#access']);

    $element = [
      '#access' => TRUE,
    ];
    RenderElement::mergeAccess($element, FALSE, FALSE);
    $this->assertSame(TRUE, $element['#access']);
  }

}
