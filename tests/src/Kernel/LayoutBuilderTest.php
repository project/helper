<?php

declare(strict_types=1);

namespace Drupal\Tests\helper\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\helper\LayoutBuilder;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;

/**
 * Tests the layout builder helper.
 *
 * @coversDefaultClass \Drupal\helper\LayoutBuilder
 * @group helper
 */
class LayoutBuilderTest extends HelperKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'layout_discovery',
    'layout_builder',
    'entity_test',
    'field',
    'system',
    'user',
  ];

  /**
   * The layout builder helper.
   *
   * @var \Drupal\helper\LayoutBuilder
   */
  protected LayoutBuilder $layoutBuilder;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('user');

    $this->layoutBuilder = $this->container->get('helper.layout_builder');
  }

  /**
   * Tests the layout builder status methods.
   *
   * @covers ::isEntityLayoutEnabled
   * @covers ::isEntityLayoutOverridden
   *
   * @dataProvider providerIsLayoutBuilderEnabled
   */
  public function testIsLayoutBuilderEnabled(bool $is_enabled, bool $is_overridable, array $section_data = []): void {
    $display = LayoutBuilderEntityViewDisplay::create([
      'targetEntityType' => 'entity_test',
      'bundle' => 'entity_test',
      'mode' => 'default',
      'status' => TRUE,
    ]);
    if ($is_enabled) {
      $display->enableLayoutBuilder();
      if ($is_overridable) {
        $display->setOverridable();
      }
    }
    $display->save();

    $entity = EntityTest::create([OverridesSectionStorage::FIELD_NAME => $section_data]);
    $entity->save();

    $this->assertSame($is_enabled, $this->layoutBuilder->isEntityLayoutEnabled($entity));
    $this->assertSame($is_enabled && $is_overridable, $this->layoutBuilder->isEntityLayoutEnabled($entity, TRUE));
    $this->assertSame($is_enabled && $is_overridable && !empty($section_data), $this->layoutBuilder->isEntityLayoutOverridden($entity));
  }

  /**
   * Provides test data for ::testIsLayoutBuilderEnabled().
   */
  public function providerIsLayoutBuilderEnabled(): array {
    $section_data = [
      new Section('layout_onecol', [], [
        'first-uuid' => new SectionComponent('first-uuid', 'content', ['id' => 'foo']),
      ]),
    ];

    return [
      [
        FALSE,
        FALSE,
      ],
      [
        TRUE,
        FALSE,
      ],
      [
        TRUE,
        TRUE,
      ],
      [
        TRUE,
        FALSE,
        $section_data,
      ],
      [
        TRUE,
        TRUE,
        $section_data,
      ],
    ];
  }

  /**
   * Test the helper for hiding core layouts.
   */
  public function testHidingCoreLayouts(): void {
    $layoutPluginManager = $this->container->get('plugin.manager.core.layout');

    // By default, the core layouts should be returned.
    $definitions = $layoutPluginManager->getFilteredDefinitions('layout_builder');
    $this->assertSame([
      'layout_onecol',
      'layout_twocol_section',
      'layout_threecol_section',
      'layout_fourcol_section',
    ], array_keys($definitions));

    // Enable hiding the core layouts.
    $this->setHelper('core_hide_layout_providers', [
      'layout_discovery',
      'layout_builder',
    ]);
    $layoutPluginManager->clearCachedDefinitions();

    $definitions = $layoutPluginManager->getFilteredDefinitions('layout_builder');
    $this->assertEmpty($definitions);
  }

}
