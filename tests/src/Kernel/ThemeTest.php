<?php

declare(strict_types=1);

namespace Drupal\Tests\helper\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the theme helper.
 *
 * @coversDefaultClass \Drupal\helper\Theme
 * @group helper
 */
class ThemeTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'helper',
  ];

  /**
   * The theme helper.
   *
   * @var \Drupal\helper\Theme
   */
  protected $themeHelper;

  /**
   * The available themes.
   *
   * @var string[]
   */
  protected array $themes;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->themeHelper = $this->container->get('helper.theme');
    $this->themes = array_keys($this->container->get('extension.list.theme')->getList());
  }

  /**
   * Test helper_system_info_alter().
   */
  public function testSubthemeRegionInheritance(): void {
    $this->container->get('theme_installer')->install([
      'test_theme',
      'helper_test_theme_regions_inherit',
      'helper_test_theme_regions_override',
    ]);

    /** @var \Drupal\Core\Extension\ThemeExtensionList $themeExtensionList */
    $themeExtensionList = $this->container->get('extension.list.theme');
    $base_theme_info = $themeExtensionList->getExtensionInfo('test_theme');
    $this->assertSame($base_theme_info['regions'], [
      'content' => 'Content',
      'left' => 'Left',
      'right' => 'Right',
    ]);

    $subtheme_info = $themeExtensionList->getExtensionInfo('helper_test_theme_regions_inherit');
    $this->assertSame($subtheme_info['regions'], $base_theme_info['regions']);

    $subtheme_info = $themeExtensionList->getExtensionInfo('helper_test_theme_regions_override');
    $this->assertNotSame($subtheme_info['regions'], $base_theme_info['regions']);
  }

  /**
   * Tests default theme detection.
   *
   * @covers ::isActive
   * @covers ::isDefault
   * @covers ::setDefault
   *
   * @dataProvider providerIsDefault
   */
  public function testIsDefault(string $default_theme, array $expected_base_themes = []): void {
    // Test the default and active theme expected results before a default
    // theme is set.
    $this->assertNull($this->container->get('theme_handler')->getDefault());
    $this->assertSame('core', $this->container->get('theme.manager')->getActiveTheme()->getName());
    $this->assertFalse($this->themeHelper->isActive($default_theme));
    $this->assertFalse($this->themeHelper->isDefault($default_theme));

    // Test that setDefaultTheme() worked as expected.
    $this->assertTrue($this->themeHelper->setDefault($default_theme));
    $this->assertSame($default_theme, $this->container->get('theme_handler')->getDefault());
    $this->assertSame($default_theme, $this->container->get('theme.manager')->getActiveTheme()->getName());
    $this->assertTrue($this->themeHelper->isActive($default_theme));
    $this->assertTrue($this->themeHelper->isDefault($default_theme));

    // Test that calling setDefaultTheme() again should return false.
    $this->assertFalse($this->themeHelper->setDefault($default_theme));

    // Ensure the expected subthemes are all valid.
    if (!empty($expected_base_themes)) {
      $this->assertEmpty(array_diff($expected_base_themes, $this->themes));
    }

    foreach ($expected_base_themes as $base_theme) {
      $this->assertTrue($this->themeHelper->isBaseTheme($base_theme, $default_theme));
    }

    foreach ($this->themes as $theme_name) {
      if ($theme_name === $default_theme) {
        $this->assertTrue($this->themeHelper->isActive($theme_name), "Asserting that isActive($theme_name) when active theme is $default_theme is TRUE");
        $this->assertTrue($this->themeHelper->isDefault($theme_name), "Asserting that isDefault($theme_name) when default theme is $default_theme is TRUE");
      }
      elseif (in_array($theme_name, $expected_base_themes, TRUE)) {
        $this->assertFalse($this->themeHelper->isActive($theme_name), "Asserting that isActive($theme_name) when active theme is $default_theme is FALSE");
        $this->assertFalse($this->themeHelper->isDefault($theme_name), "Asserting that isDefault($theme_name) when default theme is $default_theme is FALSE");
        $this->assertTrue($this->themeHelper->isActive($theme_name, TRUE), "Asserting that isActive($theme_name, TRUE) when active theme is $default_theme is TRUE");
        $this->assertTrue($this->themeHelper->isDefault($theme_name, TRUE), "Asserting that isDefault($theme_name, TRUE) when default theme is $default_theme is TRUE");
      }
      else {
        $this->assertFalse($this->themeHelper->isActive($theme_name, TRUE), "Asserting that isActive($theme_name) when active theme is $default_theme is FALSE");
        $this->assertFalse($this->themeHelper->isDefault($theme_name, TRUE), "Asserting that isDefault($theme_name) when default theme is $default_theme is FALSE");
      }
    }
  }

  /**
   * Provides test data for ::testIsDefault().
   */
  public function providerIsDefault(): array {
    $data = [];

    $data[] = [
      'stark',
      [],
    ];

    $data[] = [
      'starterkit_theme',
      [
        'stable9',
      ],
    ];

    $data[] = [
      'test_theme',
      [
        'starterkit_theme',
        'stable9',
      ],
    ];

    $data[] = [
      'test_subsubtheme',
      [
        'test_basetheme',
        'test_subtheme',
      ],
    ];

    return $data;
  }

}
