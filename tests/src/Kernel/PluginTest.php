<?php

namespace Drupal\Tests\helper\Kernel;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\filter\FilterPluginManager;
use Drupal\filter\FilterProcessResult;
use Drupal\helper\Plugin;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Tests to ensure the Plugin helper works.
 *
 * @group helper
 *
 * @coversDefaultClass \Drupal\helper\Plugin
 */
class PluginTest extends HelperKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'filter',
    'layout_discovery',
  ];

  /**
   * Tests for getManager()
   *
   * @covers ::getManager
   */
  public function testGetManager(): void {
    $manager = Plugin::getManager('filter');
    $this->assertInstanceOf(FilterPluginManager::class, $manager);

    $manager = Plugin::getManager('core.layout');
    $this->assertInstanceOf(LayoutPluginManagerInterface::class, $manager);

    // Test an invalid plugin throws an exception.
    $this->expectException(ServiceNotFoundException::class);
    $this->expectExceptionMessage('You have requested a non-existent service "plugin.manager.invalid".');
    Plugin::getManager('invalid');
  }

  /**
   * Tests for invoke()
   *
   * @covers ::invoke
   */
  public function testCall(): void {
    $result = Plugin::invoke('filter', 'filter_html_escape', [], 'process', '<p>Test markup</p>', NULL);
    $this->assertInstanceOf(FilterProcessResult::class, $result);
    $this->assertSame('&lt;p&gt;Test markup&lt;/p&gt;', $result->getProcessedText());

    $configuration = [
      'settings' => [
        'allowed_html' => '',
        'filter_html_nofollow' => FALSE,
      ],
    ];
    $result = Plugin::invoke('filter', 'filter_html', $configuration, 'process', '<p>Test markup</p>', NULL);
    $this->assertInstanceOf(FilterProcessResult::class, $result);
    $this->assertSame('Test markup', $result->getProcessedText());

    // Test an invalid plugin throws an exception.
    $this->expectException(PluginNotFoundException::class);
    Plugin::invoke('core.layout', 'invalid_plugin', [], 'getPluginDefinition');
  }

  /**
   * Tests for getOptions()
   *
   * @covers ::getOptions
   */
  public function testGetOptions(): void {
    $options = Plugin::getOptions('filter');
    // Drupal 10 has a new filter that Drupal 9 does not have.
    if (version_compare(\Drupal::VERSION, '10.1', '>=')) {
      $this->assertSame([
        'filter_align' => 'Align images',
        'filter_caption' => 'Caption images',
        'filter_autop' => 'Convert line breaks into HTML (i.e. <code>&lt;br&gt;</code> and <code>&lt;p&gt;</code>)',
        'filter_url' => 'Convert URLs into links',
        'filter_htmlcorrector' => 'Correct faulty and chopped off HTML',
        'filter_html_escape' => 'Display any HTML as plain text',
        'filter_image_lazy_load' => 'Lazy load images',
        'filter_html' => 'Limit allowed HTML tags and correct faulty HTML',
        'filter_html_image_secure' => 'Restrict images to this site',
      ], $options);
    }
    else {
      $this->assertSame([
        'filter_align' => 'Align images',
        'filter_caption' => 'Caption images',
        'filter_autop' => 'Convert line breaks into HTML (i.e. <code>&lt;br&gt;</code> and <code>&lt;p&gt;</code>)',
        'filter_url' => 'Convert URLs into links',
        'filter_htmlcorrector' => 'Correct faulty and chopped off HTML',
        'filter_html_escape' => 'Display any HTML as plain text',
        'filter_html' => 'Limit allowed HTML tags and correct faulty HTML',
        'filter_html_image_secure' => 'Restrict images to this site',
      ], $options);
    }

    // This time with the fallback option turned on.
    $options = Plugin::getOptions('filter', NULL, ['fallback' => TRUE]);
    if (version_compare(\Drupal::VERSION, '10.1', '>=')) {
      $this->assertSame([
        'filter_align' => 'Align images',
        'filter_caption' => 'Caption images',
        'filter_autop' => 'Convert line breaks into HTML (i.e. <code>&lt;br&gt;</code> and <code>&lt;p&gt;</code>)',
        'filter_url' => 'Convert URLs into links',
        'filter_htmlcorrector' => 'Correct faulty and chopped off HTML',
        'filter_html_escape' => 'Display any HTML as plain text',
        'filter_image_lazy_load' => 'Lazy load images',
        'filter_html' => 'Limit allowed HTML tags and correct faulty HTML',
        'filter_null' => 'Provides a fallback for missing filters. Do not use.',
        'filter_html_image_secure' => 'Restrict images to this site',
      ], $options);
    }
    else {
      $this->assertSame([
        'filter_align' => 'Align images',
        'filter_caption' => 'Caption images',
        'filter_autop' => 'Convert line breaks into HTML (i.e. <code>&lt;br&gt;</code> and <code>&lt;p&gt;</code>)',
        'filter_url' => 'Convert URLs into links',
        'filter_htmlcorrector' => 'Correct faulty and chopped off HTML',
        'filter_html_escape' => 'Display any HTML as plain text',
        'filter_html' => 'Limit allowed HTML tags and correct faulty HTML',
        'filter_null' => 'Provides a fallback for missing filters. Do not use.',
        'filter_html_image_secure' => 'Restrict images to this site',
      ], $options);
    }

    $options = Plugin::getOptions('core.layout');
    $this->assertSame([
      'Columns: 1' => [
        'layout_onecol' => 'One column',
      ],
      'Columns: 2' => [
        'layout_twocol' => 'Two column',
        'layout_twocol_bricks' => 'Two column bricks',
      ],
      'Columns: 3' => [
        'layout_threecol_25_50_25' => 'Three column 25/50/25',
        'layout_threecol_33_34_33' => 'Three column 33/34/33',
      ],
    ], $options);

    $options = Plugin::getOptions('block');
    $this->assertSame([
      'Content' => [
        'helper_node_field' => 'Node field',
      ],
      'Core' => [
        'page_title_block' => 'Page title',
        'local_actions_block' => 'Primary admin actions',
        'local_tasks_block' => 'Tabs',
      ],
    ], $options);
  }

}
