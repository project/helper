<?php

namespace Drupal\Tests\helper\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the Twig extension.
 *
 * @coversDefaultClass \Drupal\helper\Twig\HelperExtension
 * @group helper
 */
class TwigExtensionTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'file',
    'helper',
  ];

  /**
   * The Twig environment.
   *
   * @var \Twig\Environment
   */
  protected $twig;

  /**
   * The file helper.
   *
   * @var \Drupal\helper\File
   */
  protected $fileHelper;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->twig = $this->container->get('twig');
    $this->fileHelper = $this->container->get('helper.file');
  }

  /**
   * Tests that the twig function was registered correctly.
   *
   * @covers ::getFunctions
   */
  public function testTwigExtension(): void {
    $this->assertNotFalse($this->twig->getFunction('file_data_uri'));
  }

  /**
   * Tests the file_data_uri() Twig function.
   *
   * @covers ::fileDataUri
   */
  public function testFileDataUri(): void {
    $template = '<img src="{{ file_data_uri(uri) }}" />';
    $uri = DRUPAL_ROOT . '/core/misc/favicon.ico';
    $expected = $this->fileHelper->getDataUri($uri);
    $this->assertEquals($this->twig->renderInline($template, ['uri' => $uri]), '<img src="' . $expected . '" />');

    $template = '<img src="{{ file_data_uri(uri, true, "test/mime") }}" />';
    $uri = DRUPAL_ROOT . '/core/misc/favicon.ico';
    $expected = $this->fileHelper->getDataUri($uri, TRUE, 'test/mime');
    $this->assertEquals($this->twig->renderInline($template, ['uri' => $uri]), '<img src="' . $expected . '" />');

    $uri = 'public://invalid.png';
    $this->assertEquals($this->twig->renderInline($template, ['uri' => $uri]), '<img src="" />');
  }

}
