<?php

declare(strict_types=1);

namespace Drupal\Tests\helper\Kernel\File;

use Drupal\helper\File\ComposerFile;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the Composer helper.
 *
 * @coversDefaultClass \Drupal\helper\File\ComposerFile
 * @group helper
 */
class ComposerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'helper',
  ];

  /**
   * Test getting the Drupal composer.json contents.
   *
   * @covers ::getJson
   * @covers ::writeJson
   */
  public function testComposer(): void {
    $composer = new ComposerFile();
    $data = $composer->readData();
    $this->assertIsArray($data);
    $this->assertArrayHAsKey('require', $data);
    $this->assertNotEmpty($data['require']);

    $composer = new ComposerFile('vendor/composer/installed.json');
    $data = $composer->readData();
    $this->assertIsArray($data);
    $this->assertArrayHasKey('packages', $data);
    $this->assertNotEmpty($data['packages']);

    // Write a new empty composer.json file into the files directory.
    $directory = $this->getActualDirectory();
    $composer = new ComposerFile($directory . '/composer.json');
    file_put_contents($directory . '/composer.json', '');
    $composer->writeData([]);
    $this->assertSame('[]', file_get_contents($directory . '/composer.json'));
    $data = $composer->readData();
    $this->assertSame([], $data);

    $composer->writeData(['updated' => TRUE]);
    $this->assertSame("{\n    \"updated\": true\n}", file_get_contents($directory . '/composer.json'));
    $data = $composer->readData();
    $this->assertSame(['updated' => TRUE], $data);
  }

  /**
   * Test reading a file that doesn't exist yet.
   *
   * @covers ::getJson
   */
  public function testFileNotExists() {
    $directory = $this->getActualDirectory();
    $composer = new ComposerFile($directory);
    $this->expectException(\RuntimeException::class);
    $this->expectExceptionMessage("The file {$directory}/composer.json does not exist or is not readable.");
    $composer->readData();
  }

  /**
   * Test reading an unreadable JSON file.
   *
   * @covers ::getJson
   * @covers ::writeJson
   */
  public function testNotReadable() {
    $directory = $this->getActualDirectory();
    file_put_contents($directory . '/composer.json', '{}');
    chmod($directory . '/composer.json', 0000);
    $this->expectException(\RuntimeException::class);
    $this->expectExceptionMessage("The file {$directory}/composer.json does not exist or is not readable.");
    $composer = new ComposerFile($directory);
    $composer->readData();
  }

  /**
   * Test reading an invalid JSON file.
   *
   * @covers ::getJson
   * @covers ::writeJson
   */
  public function testInvalidJson(): void {
    $directory = $this->getActualDirectory();
    $composer = new ComposerFile($directory);
    $composer->writeData('invalid');
    $this->expectException(\JsonException::class);
    $composer->readData();
  }

  /**
   * Returns the actual site directory of the test.
   *
   * Needed for testing exec/passthru commands.
   *
   * @return string
   *   The directory.
   */
  private function getActualDirectory(): string {
    // Use a sub-directory to ensure we don't mess with any other files.
    $directory = $this->root . str_replace('vfs://root', '', $this->siteDirectory) . '/composer-test';
    // Ensure the directory exists and in writable.
    mkdir($directory, 0777, TRUE);
    return $directory;
  }

  /**
   * Test the exec() function.
   *
   * @covers ::exec
   * @covers ::getJson
   */
  public function testExec(): void {
    $directory = $this->getActualDirectory();
    $this->assertDirectoryExists($directory);
    $this->assertDirectoryIsWritable($directory);
    $this->assertFileDoesNotExist($directory . '/composer.json');

    file_put_contents($directory . '/composer.json', '{}');
    $this->assertFileExists($directory . '/composer.json');

    $composer = new ComposerFile($directory);
    $data = $composer->readData();
    $this->assertSame([], $data);

    $composer->exec('config --json extra.test true', $result_code);
    $this->assertSame(0, $result_code);

    $data = $composer->readData();
    $this->assertSame(['extra' => ['test' => TRUE]], $data);
  }

  /**
   * Test the exec() function without having a composer.json in the directory.
   *
   * @covers ::exec
   */
  public function testExecWithInaccessibleDirectory(): void {
    $directory = $this->getActualDirectory();
    chmod($directory, 0444);
    $this->assertDirectoryExists($directory);
    $this->assertDirectoryIsNotWritable($directory);

    $this->expectException(\RuntimeException::class);
    $this->expectExceptionMessage("The file {$directory}/composer.json does not exist or is not writable.");
    $composer = new ComposerFile($directory);
    $composer->exec('config --json extra.test true', $result_code);
    $this->assertNotSame(0, $result_code);
  }

  /**
   * Test the exec() function without having a composer.json in the directory.
   *
   * @covers ::exec
   */
  public function testExecWithInaccessibleFile(): void {
    $directory = $this->getActualDirectory();
    file_put_contents($directory . '/composer.json', '{}');
    chmod($directory . '/composer.json', 0444);
    $this->assertDirectoryExists($directory);
    $this->assertDirectoryIsWritable($directory);
    $this->assertFileExists($directory . '/composer.json');
    $this->assertFileIsNotWritable($directory . '/composer.json');

    $this->expectException(\RuntimeException::class);
    $this->expectExceptionMessage("The file {$directory}/composer.json does not exist or is not writable.");
    $composer = new ComposerFile($directory);
    $composer->exec('config --json extra.test true', $result_code);
    $this->assertNotSame(0, $result_code);
  }

}
