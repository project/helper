<?php

declare(strict_types=1);

namespace Drupal\Tests\helper\Kernel\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Site\Settings;
use Drupal\helper\Entity\ContentEntityUpdater;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests \Drupal\helper\Entity\ContentEntityUpdater.
 *
 * @coversDefaultClass \Drupal\helper\Entity\ContentEntityUpdater
 * @group helper
 */
class ContentEntityUpdaterTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_test',
    'helper',
    'user',
    'system',
  ];

  /**
   * The content entity updater.
   *
   * @var \Drupal\helper\Entity\ContentEntityUpdater
   */
  protected ContentEntityUpdater $updater;

  /**
   * The update sandbox array.
   *
   * @var array
   */
  protected array $sandbox = [];

  /**
   * The default entity update batch size.
   *
   * @var int
   */
  protected int $batchSize = 10;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('entity_test');

    // Setup the entity update batch size setting to a default of 10.
    $settings = Settings::getInstance() ? Settings::getAll() : [];
    $settings['entity_update_batch_size'] = $this->batchSize;
    new Settings($settings);

    $this->updater = $this->container->get('class_resolver')->getInstanceFromDefinition(ContentEntityUpdater::class);
  }

  /**
   * Create test entities.
   *
   * @param int $count
   *   The number of test entities to create.
   * @param array $data
   *   The entity data.
   * @param \Drupal\Core\Entity\EntityStorageInterface|null $storage
   *   The entity storage object.
   */
  protected function createEntities(int $count, array $data = [], ?EntityStorageInterface &$storage = NULL): void {
    if (!isset($storage)) {
      $storage = $this->container->get('entity_type.manager')->getStorage('entity_test');
    }
    $label_key = $storage->getEntityType()->getKey('label');
    for ($i = 1; $i <= $count; $i++) {
      $label = $storage->getEntityTypeId() . '_' . $i;
      $storage->create($data + [$label_key => $label])->save();
    }
  }

  /**
   * Test the entity updater.
   *
   * @covers ::update
   * @covers ::doOne
   */
  public function testUpdate(): void {
    // Create some entities to update.
    $count = 15;
    $this->createEntities($count, [], $storage);

    $callback = function (EntityInterface $entity) {
      /** @var \Drupal\entity_test\Entity\EntityTest $entity */
      $number = (int) $entity->id();
      // Only update even numbered entities.
      if ($number % 2 === 0) {
        $entity->setName($entity->getName() . ' (updated)');
        return TRUE;
      }
      return FALSE;
    };

    // This should run against the first 10 entities. The even numbered labels
    // will have been updated.
    $this->updater->update($this->sandbox, 'entity_test', $callback);
    $entities = $storage->loadMultiple();
    $this->assertCount($count, $entities);
    $this->assertSame('entity_test_8 (updated)', $entities[8]->label());
    $this->assertSame('entity_test_9', $entities[9]->label());
    $this->assertSame('entity_test_10 (updated)', $entities[10]->label());
    $this->assertSame('entity_test_12', $entities[12]->label());
    $this->assertSame('entity_test_13', $entities[13]->label());
    $this->assertSame('entity_test_14', $entities[14]->label());
    $this->assertSame($count, $this->sandbox['content_entity_updater']['count']);
    $this->assertSame('entity_test', $this->sandbox['content_entity_updater']['entity_type']);
    $this->assertSame($this->batchSize / $count, $this->sandbox['#finished']);
    $this->assertCount(5, $this->sandbox['content_entity_updater']['entities']);

    // Update the rest.
    $this->updater->update($this->sandbox, 'entity_test', $callback);
    $entities = $storage->loadMultiple();
    $this->assertSame('entity_test_8 (updated)', $entities[8]->label());
    $this->assertSame('entity_test_9', $entities[9]->label());
    $this->assertSame('entity_test_10 (updated)', $entities[10]->label());
    $this->assertSame('entity_test_12 (updated)', $entities[12]->label());
    $this->assertSame('entity_test_13', $entities[13]->label());
    $this->assertSame('entity_test_14 (updated)', $entities[14]->label());
    $this->assertSame($count, $this->sandbox['content_entity_updater']['count']);
    $this->assertSame('entity_test', $this->sandbox['content_entity_updater']['entity_type']);
    $this->assertSame(1, $this->sandbox['#finished']);
    $this->assertCount(0, $this->sandbox['content_entity_updater']['entities']);
  }

  /**
   * Test providing an entity type and single bundle name to the updater.
   *
   * @covers ::update
   * @covers ::doOne
   */
  public function testUpdateBundle(): void {
    $this->createEntities(2, ['type' => 'type1', 'name' => 'entity_test_type1'], $storage);
    $this->createEntities(2, ['type' => 'type2', 'name' => 'entity_test_type2'], $storage);

    $this->updater->update($this->sandbox, 'entity_test:type1', static function (EntityInterface $entity) {
      /** @var \Drupal\entity_test\Entity\EntityTest $entity */
      $entity->setName($entity->getName() . ' (updated)');
      return TRUE;
    });

    $entities = $storage->loadMultiple();
    $this->assertSame('entity_test_type1 (updated)', $entities[1]->label());
    $this->assertSame('entity_test_type1 (updated)', $entities[2]->label());
    $this->assertSame('entity_test_type2', $entities[3]->label());
    $this->assertSame('entity_test_type2', $entities[4]->label());
  }

  /**
   * Test providing an entity type and multiple bundle names to the updater.
   *
   * @covers ::update
   * @covers ::doOne
   */
  public function testUpdateBundles(): void {
    $this->createEntities(2, ['type' => 'type1', 'name' => 'entity_test_type1'], $storage);
    $this->createEntities(2, ['type' => 'type2', 'name' => 'entity_test_type2'], $storage);
    $this->createEntities(2, ['type' => 'type3', 'name' => 'entity_test_type3'], $storage);

    $this->updater->update($this->sandbox, 'entity_test:type1:type3', static function (EntityInterface $entity): bool {
      /** @var \Drupal\entity_test\Entity\EntityTest $entity */
      $entity->setName($entity->getName() . ' (updated)');
      return TRUE;
    });

    $entities = $storage->loadMultiple();
    $this->assertSame('entity_test_type1 (updated)', $entities[1]->label());
    $this->assertSame('entity_test_type1 (updated)', $entities[2]->label());
    $this->assertSame('entity_test_type2', $entities[3]->label());
    $this->assertSame('entity_test_type2', $entities[4]->label());
    $this->assertSame('entity_test_type3 (updated)', $entities[5]->label());
    $this->assertSame('entity_test_type3 (updated)', $entities[6]->label());
  }

  /**
   * Test providing an entity query object to the updater.
   *
   * @covers ::update
   * @covers ::doOne
   */
  public function testUpdateQuery(): void {
    $this->createEntities(3, [], $storage);

    // Test passing an entity query instead of the entity ID string.
    $query = $storage->getQuery();
    $query->condition('id', 2);
    $query->accessCheck(FALSE);

    $this->updater->update($this->sandbox, $query, static function (EntityInterface $entity): bool {
      /** @var \Drupal\entity_test\Entity\EntityTest $entity */
      $entity->setName($entity->getName() . ' (updated)');
      return TRUE;
    });

    $entities = $storage->loadMultiple();
    $this->assertSame('entity_test_1', $entities[1]->label());
    $this->assertSame('entity_test_2 (updated)', $entities[2]->label());
    $this->assertSame('entity_test_3', $entities[3]->label());
  }

  /**
   * Test an exception is thrown when updating a config entity type.
   *
   * @covers ::update
   */
  public function testUpdateException(): void {
    $this->expectException(\InvalidArgumentException::class);
    $this->expectExceptionMessage('The provided entity type ID \'entity_test_bundle\' is not a content entity type');
    $this->updater->update($this->sandbox, 'entity_test_bundle', static function (EntityInterface $entity) {});
  }

  /**
   * Test an exception is thrown when updating multiple entity types.
   *
   * @covers ::update
   */
  public function testUpdateOncePerUpdateException(): void {
    $this->updater->update($this->sandbox, 'entity_test', static function (EntityInterface $entity) {});
    $this->expectException(\RuntimeException::class);
    $this->expectExceptionMessage('Updating multiple entity types in the same update function is not supported');
    $this->updater->update($this->sandbox, 'entity_test_label', static function (EntityInterface $entity) {});
  }

}
