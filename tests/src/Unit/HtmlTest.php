<?php

declare(strict_types=1);

namespace Drupal\Tests\helper\Unit;

use Drupal\helper\Html;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the HTML helper.
 *
 * @coversDefaultClass \Drupal\helper\Html
 * @group helper
 */
class HtmlTest extends UnitTestCase {

  /**
   * Test the removeComments method.
   *
   * @covers ::removeComments
   * @dataProvider providerRemoveComments
   */
  public function testRemoveComments(string $value, string $expected): void {
    $result = Html::removeComments($value);
    $this->assertSame($expected, $result);
  }

  /**
   * Provides test data for ::testRemoveComments().
   */
  public function providerRemoveComments(): array {
    return [
      'twig-debug' => [
        'markup' => "<!-- THEME DEBUG -->
<!-- THEME HOOK: 'test' -->
<!-- FILE NAME SUGGESTIONS:
   * test.html.twig
-->
<!-- BEGIN OUTPUT from 'helper/test.html.twig' -->

<test><!-- <removed> --><!----></test>

<!-- END OUTPUT from 'helper/test.html.twig' -->",
        'expected' => "<test></test>\n\n",
      ],
      // Test that even conditional comments are removed.
      'conditionals' => [
        'markup' => "<!-- a comment --><!--[if !lte IE 6]><!-->
	<link rel=\"stylesheet\" href=\"/css/style.css\" />
<!--<![endif]--><!-- a comment -->",
        'expected' => "<link rel=\"stylesheet\" href=\"/css/style.css\" />\n",
      ],
    ];
  }

}
