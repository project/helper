<?php

declare(strict_types=1);

namespace Drupal\Tests\helper\Unit;

use Drupal\helper\Utility;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the utility helper.
 *
 * @coversDefaultClass \Drupal\helper\Utility
 * @group helper
 */
class UtilityTest extends UnitTestCase {

  /**
   * Test the camelCase method.
   *
   * @covers ::camelCase
   * @dataProvider providerCamelCase
   */
  public function testCamelCase(string $value, string $expected, bool $lower = FALSE): void {
    $result = Utility::camelCase($value, $lower);
    $this->assertSame($expected, $result);
  }

  /**
   * Provides test data for ::testCamelCase().
   */
  public function providerCamelCase(): array {
    return [
      [
        'sample-value',
        'SampleValue',
      ],
      [
        'This is a full sentence. With special characters!?',
        'thisIsAFullSentenceWithSpecialCharacters',
        TRUE,
      ],
    ];
  }

}
