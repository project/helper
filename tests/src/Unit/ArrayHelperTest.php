<?php

declare(strict_types=1);

namespace Drupal\Tests\helper\Unit;

use Drupal\helper\ArrayHelper;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the array helper.
 *
 * @coversDefaultClass \Drupal\helper\ArrayHelper
 * @group helper
 */
class ArrayHelperTest extends UnitTestCase {

  /**
   * Test the hasValue method.
   *
   * @covers ::hasValue
   * @dataProvider providerHasValue
   */
  public function testHasValue(array $array, mixed $value, bool $expected_result, array $expected_keys, bool $strict = TRUE): void {
    $result = ArrayHelper::hasValue($array, $value, $strict, $keys);
    $this->assertSame($expected_result, $result);
    $this->assertSame($expected_keys, $keys);
  }

  /**
   * Provides test data for ::testHasValue().
   */
  public function providerHasValue(): array {
    return [
      'single-result' => [
        ['a', 'b', 'c', 'd'],
        'a',
        TRUE,
        [0],
      ],
      'multiple-results' => [
        ['a', 'b', 'c', 'd', 'd'],
        'd',
        TRUE,
        [3, 4],
      ],
      'not-found' => [
        ['a', 'b', 'c'],
        'd',
        FALSE,
        [],
      ],
      'not-strict-result' => [
        ['first', 0, '0', FALSE, '', 'last'],
        NULL,
        TRUE,
        [1, 3, 4],
        FALSE,
      ],
    ];
  }

  /**
   * Test the replaceValue method.
   *
   * @covers ::replaceValue
   * @dataProvider providerReplaceValue
   */
  public function testReplaceValue(array $array, mixed $search, mixed $replace, array $expected_array, bool $strict = TRUE): void {
    ArrayHelper::replaceValue($array, $search, $replace, $strict);
    $this->assertSame($expected_array, $array);
  }

  /**
   * Provides test data for ::testReplaceValues().
   */
  public function providerReplaceValue(): array {
    return [
      'single-result' => [
        ['a', 'b', 'c', 'd'],
        'a',
        'b',
        ['b', 'b', 'c', 'd'],
      ],
      'multiple-results' => [
        ['a', 'b', 'c', 'd', 'd'],
        'd',
        'a',
        ['a', 'b', 'c', 'a', 'a'],
      ],
      'not-found' => [
        ['a', 'b', 'c'],
        'd',
        'e',
        ['a', 'b', 'c'],
      ],
      'not-strict-result' => [
        ['first', 0, '0', FALSE, '', 'last'],
        NULL,
        'empty',
        ['first', 'empty', '0', 'empty', 'empty', 'last'],
        FALSE,
      ],
    ];
  }

  /**
   * Test the mapKeys method.
   *
   * @covers ::mapKeys
   */
  public function testMapKeys(): void {
    $input = [
      'a' => 1,
      'b' => '1',
      'c' => 1,
      0 => '2',
      1 => 2,
    ];
    $expected = [
      '#a' => 1,
      '#b' => '1',
      '#c' => 1,
      '#0' => '2',
      '#1' => 2,
    ];

    $result = ArrayHelper::mapKeys($input, static function ($value) {
      return '#' . $value;
    });
    $this->assertSame($result, $expected);
  }

  /**
   * Test the filterKeys method.
   *
   * @covers ::filterKeys
   */
  public function testFilterKeys(): void {
    $input = array_fill_keys(range(0, 10), 'test');
    $expected1 = array_fill_keys([0, 3, 6, 9], 'test');
    $expected2 = array_fill_keys([1, 2, 4, 5, 7, 8, 10], 'test');

    $result = ArrayHelper::filterKeys($input, static function ($value) {
      return $value % 3 === 0;
    });
    $this->assertSame($result, $expected1);

    $result = ArrayHelper::filterKeys($input, static function ($value) {
      return $value % 3;
    });
    $this->assertSame($result, $expected2);
  }

  /**
   * Test the chunkEvenly method.
   *
   * @covers ::chunkEvenly
   */
  public function testChunkEvenly(): void {
    $input = array_combine(range(10, 1, -1), range(1, 10));

    $result = ArrayHelper::chunkEvenly($input, 3);
    $this->assertSame($result, [
      0 => [1, 2, 3, 4],
      1 => [5, 6, 7],
      2 => [8, 9, 10],
    ]);

    $result = ArrayHelper::chunkEvenly($input, 4, TRUE);
    $this->assertSame($result, [
      0 => [
        10 => 1,
        9 => 2,
        8 => 3,
      ],
      1 => [
        7 => 4,
        6 => 5,
        5 => 6,
      ],
      2 => [
        4 => 7,
        3 => 8,
      ],
      3 => [
        2 => 9,
        1 => 10,
      ],
    ]);

    // Test using $num greater than items in the array.
    $result = ArrayHelper::chunkEvenly($input, 11);
    $this->assertSame($result, [
      0 => [1],
      1 => [2],
      2 => [3],
      3 => [4],
      4 => [5],
      5 => [6],
      6 => [7],
      7 => [8],
      8 => [9],
      9 => [10],
    ]);

    // Test empty array.
    $this->assertSame(ArrayHelper::chunkEvenly([], 1), []);
    $this->assertSame(ArrayHelper::chunkEvenly([], 5), []);

    // Test $num less than 1.
    $this->expectException(\DomainException::class);
    $this->expectExceptionMessage('Cannot call Drupal\helper\ArrayHelper::chunkEvenly with $chunks being less than one.');
    $this->assertSame(ArrayHelper::chunkEvenly($input, 0), []);
  }

}
