# Theme helpers

## Sub-theme region inheritance

Drupal does not support sub-themes inheriting the regions from its base theme out of the box (see https://www.drupal.org/node/225125).

If you are using the Helper module and would like to opt-in to your sub-theme inheriting its base theme's regions, add the `inherit_regions: true` flag to your `.info.yml` file:

```yaml
name: My Example Sub-Theme
type: theme
base theme: my_base_theme
inherit_regions: true
```
