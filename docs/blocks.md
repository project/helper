# Blocks

## Node field

Provides the ability to output a node's field with a block in the `Block layout` configuration. Node blocks by default can only be used within Layout Builder, but sometimes we may want to output the current node's field (like footnotes, or legal text) in a block region that may be _outside_ of the content area where the node usually renders.
