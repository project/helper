# Form Elements

## Entity Select

Creates a select field that allows the user to select from a specific entity of a configured type. Best used for configuration entities and not content entities.

> Consider the number of entity results when using this form element. It might be better to use an autocomplete instead if there could potentially be a lot of options.

```php
// Let the user select a menu:
$form['menu'] = [
  '#type' => 'helper_entity_select',
  '#target_type' => 'menu',
];

// Let the user select an article node:
$form['article'] = [
  '#type' => 'helper_entity_select',
  '#target_type' => 'node',
  '#target_bundles' => ['article'],
];
```

Note: configuration entities will include disable entities in the select list. See https://www.drupal.org/project/drupal/issues/3367492 for a patch to Drupal core to resolve this.
