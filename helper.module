<?php

/**
 * @file
 * Main file for the Helper module.
 */

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Extension\Extension;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\helper\LayoutBuilder;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;

/**
 * Checks if a helper is enabled.
 *
 * @param string $key
 *   The helper config key.
 * @param mixed $default
 *   THe default if the helper is not yet set.
 *
 * @return mixed
 *   TRUE if the helper is enabled or not.
 */
function _helper_is_enabled(string $key, mixed $default = TRUE): mixed {
  $enabled = &drupal_static(__FUNCTION__);
  if (!isset($enabled)) {
    $enabled = \Drupal::config('helper.settings')->get('enabled') ?? [];
  }
  return $enabled[$key] ?? $default;
}

/**
 * Implements hook_entity_operation().
 */
function helper_entity_operation(EntityInterface $entity) {
  $operations = [];

  if (_helper_is_enabled('layout_builder_entity_operation') && $entity instanceof FieldableEntityInterface && \Drupal::moduleHandler()->moduleExists('layout_builder')) {
    $entity_type_id = $entity->getEntityTypeId();
    $url = Url::fromRoute(
      'layout_builder.overrides.' . $entity_type_id . '.view',
      [$entity_type_id => $entity->id()]
    );
    if ($url->access()) {
      $operations['layout'] = [
        'title' => t('Layout'),
        'weight' => 50,
        'url' => $url,
      ];
    }
  }

  return $operations;
}

/**
 * Implements hook_system_info_alter().
 */
function helper_system_info_alter(array &$info, Extension $file, $type) {
  if ($type === 'theme') {
    // Keep static data of all the themes we've seen so far.
    $theme_info = &drupal_static(__FUNCTION__, []);
    $theme_info[$file->getName()] = &$info;

    // Allow themes to inherit info from their base theme (like regions).
    // @see https://www.drupal.org/node/225125
    if (isset($info['base theme'], $theme_info[$info['base theme']]) && !empty($info['inherit_regions'])) {
      // Read base theme info from static cache since we cannot use the
      // extension.list.theme service since it would cause recursion.
      $base_theme_info = $theme_info[$info['base theme']];
      assert(isset($base_theme_info['regions']) && is_array($base_theme_info['regions']));
      $info['regions'] = $base_theme_info['regions'];
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function helper_form_entity_view_display_layout_builder_form_alter(&$form): void {
  // Add validation to view mode layout builder default forms.
  $form['#validate'][] = '_helper_form_validate_no_inline_blocks';
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function helper_form_layout_layout_builder_form_alter(&$form): void {
  // Add validation to layout_library entity layout builder forms.
  $form['#validate'][] = '_helper_form_validate_no_inline_blocks';
}

/**
 * Form validation; prevent saving a layout builder form with inline blocks.
 */
function _helper_form_validate_no_inline_blocks(array &$form, FormStateInterface $form_state): void {
  if (_helper_is_enabled('layout_builder_inline_block_validation')) {
    /** @var \Drupal\layout_builder\Plugin\SectionStorage\DefaultsSectionStorage $section_storage */
    $section_storage = $form['layout_builder']['#section_storage'];
    if (\Drupal::moduleHandler()->moduleExists('block_content') && LayoutBuilder::sectionListGetComponentsByPluginId($section_storage, 'inline_block:')) {
      $form_state->setError($form['layout_builder'], t('The layout cannot contain inline blocks.'));
    }

    $plugins = LayoutBuilder::extractSectionListPlugins($section_storage, TRUE);
    foreach ($plugins as $plugin_type_plugins) {
      foreach ($plugin_type_plugins as $plugin) {
        if ($plugin instanceof DependentPluginInterface) {
          $dependencies = $plugin->calculateDependencies();
          if (!empty($dependencies['content'])) {
            $form_state->setError($form['layout_builder'], t('The layout contains blocks or layouts that depend on content. These cannot be used for default layouts.'));
          }
        }
      }
    }
  }
}

/**
 * Implements hook_plugin_filter_TYPE_alter().
 */
function helper_plugin_filter_block_alter(array &$definitions, array $extra, $consumer): void {
  if (_helper_is_enabled('layout_builder_inline_block_validation')) {
    if ($consumer === 'layout_builder' && isset($extra['section_storage']) && !($extra['section_storage'] instanceof OverridesSectionStorage)) {
      foreach ($definitions as $plugin_id => $definition) {
        if (str_starts_with($plugin_id, 'inline_block:')) {
          unset($definitions[$plugin_id]);
        }
      }
    }
  }

  // Remove any blocks that are marked as hidden or deprecated.
  // @todo Should this be configurable?
  foreach ($definitions as $key => $definition) {
    if (!empty($definition['hidden']) || !empty($definition['deprecated'])) {
      unset($definitions[$key]);
    }
  }
}

/**
 * Implements hook_plugin_filter_TYPE_alter().
 */
function helper_plugin_filter_layout_alter(array &$definitions): void {
  // Remove any layouts that are marked as hidden or deprecated.
  // @todo Should this be configurable?
  /** @var \Drupal\Core\Layout\LayoutDefinition $definition */
  foreach ($definitions as $key => $definition) {
    if ($definition->get('hidden') || $definition->get('deprecated')) {
      unset($definitions[$key]);
    }
  }
}

/**
 * Implements hook_layout_alter().
 */
function helper_layout_alter(array &$definitions) {
  // Set requested provider layouts as hidden.
  if ($hidden_providers = _helper_is_enabled('core_hide_layout_providers', [])) {
    assert(is_array($hidden_providers));
    /** @var \Drupal\Core\Layout\LayoutDefinition $definition */
    foreach ($definitions as $definition) {
      if (in_array($definition->getProvider(), $hidden_providers, TRUE)) {
        $definition->set('hidden', TRUE);
      }
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function helper_form_alter(array &$form) {
  // Disable HTML5 validation.
  if (_helper_is_enabled('core_form_novalidate', FALSE)) {
    $form['#attributes']['novalidate'] = TRUE;
  }
}

/**
 * Implements hook_widget_info_alter().
 */
function helper_field_widget_info_alter(&$info) {
  if (_helper_is_enabled('core_text_textarea_widgets', FALSE)) {
    $info['text_textarea']['field_types'][] = 'text';
    $info['string_textarea']['field_types'][] = 'string';
  }
}
